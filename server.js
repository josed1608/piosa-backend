//
// Base server configuration file

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var router = express.Router();
var expressValidator = require('express-validator');

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type, Accept");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date,' +
        ' X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization, X-Requested-With');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// log requests to the console
app.use(morgan('dev'));

// express validator
app.use(expressValidator());

// set our port
var port = process.env.PORT || 8081;

// ROUTING

// middleware to use for all requests
router.use(function (req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

// Method to diagnose the request method used in every request
router.use('/', function (req, res, next) {
    console.log('Request Type is: ', req.method);
    next();
});

router.get('/', function (req, res) {
    res.json({message: 'hooray! welcome to our base web service!'});
});


// REGISTER OUR ROUTES -------------------------------
app.use('/api', router);

var admin_user_ws = require('./routes/admin_users/admin_user_ws');
var admin_type_ws = require('./routes/admin_users/admin_type_ws');
var entity_ws = require('./routes/admin_users/entity_ws');
var admin_log_ws = require('./routes/admin_users/admin_log_ws');
var admin_role_ws = require('./routes/admin_users/admin_role_ws');
var role_type_ws = require('./routes/admin_users/role_type_ws');
// var user_ws = require('./routes/users/user_ws');

app.use('/api/adminUser', admin_user_ws);
app.use('/api/adminType', admin_type_ws);
app.use('/api/entity', entity_ws);
app.use('/api/adminLog', admin_log_ws);
app.use('/api/adminRole', admin_role_ws);
app.use('/api/roleType', role_type_ws);
// app.use('/api/user', user_ws);

// PROYECT
var project_ws = require('./routes/project/project_ws');
var project_type_ws = require('./routes/project/project_type_ws');
var advance_ws = require('./routes/project/advance_ws');
// var announcement_ws = require('./routes/project/announcement_ws');
var event_ws = require('./routes/project/event_ws');
var article_ws = require('./routes/project/article_ws');

var project_coordinator_ws = require('./routes/project/project_coordinator_ws');
var project_collaborator_ws = require('./routes/project/project_collaborator_ws');
var professor_academic_unit_ws = require('./routes/project/professor_academic_unit_ws');
var professor_location_ws = require('./routes/project/project_location_ws');
var academic_unit_ws = require('./routes/project/academic_unit_ws');

app.use('/api/project', project_ws);
app.use('/api/projectType', project_type_ws);
app.use('/api/advance', advance_ws);
// app.use('/api/announcement', announcement_ws);w
app.use('/api/event', event_ws);
app.use('/api/article', article_ws);

app.use('/api/projectCoordinator', project_coordinator_ws);
app.use('/api/projectCollaborator', project_collaborator_ws);
app.use('/api/projectLocation', professor_location_ws);
app.use('/api/professorAcademicUnit', professor_academic_unit_ws);
app.use('/api/academicUnit', academic_unit_ws);

// SYSTEM USERS

var assistant_ws = require('./routes/team_members/assistant_ws');
var professor_ws = require('./routes/team_members/professor_ws');
var social_actor_ws = require('./routes/team_members/social_actor_ws');

app.use('/api/assistant', assistant_ws);
app.use('/api/professor', professor_ws);
app.use('/api/socialActor', social_actor_ws);


// WORK TEAM

var work_team_ws = require('./routes/team_members/work_team_ws');
var work_team_professor_ws = require('./routes/team_members/work_team_professor_ws');
var work_team_assistant_ws = require('./routes/team_members/work_team_assistant_ws');

app.use('/api/workTeam', work_team_ws);
app.use('/api/workTeamProfessor', work_team_professor_ws);
app.use('/api/workTeamAssistant', work_team_assistant_ws);

// MAIL SENDER

var mail_sender_ws = require('./routes/util/mail_sender_ws');

app.use('/api/mailSender', mail_sender_ws);

module.exports = router;

// START THE SERVER
app.listen(port);

console.log('Magic happens on port ' + port);
