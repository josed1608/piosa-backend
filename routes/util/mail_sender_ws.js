var nodemailer = require('nodemailer');
var express = require('express');
var router = express.Router();
var util = require('util');
var fs = require('fs');
var path = require('path');

/**
 * Testing method for the current entity
 */
router.get('/', function (req, res) {
    res.json({message: 'TeamLineup web service'});
});

/**
 * Sends a mail to the mail address received from param
 */
router.get('/test/:mailTo', function (req, res) {
    const options = {
        auth: {
            user: "admin.piosa@ucr.ac.cr",
            pass: "hGfbgfrx"
        }, from: "admin.piosa@ucr.ac.cr",
        to: req.params.mailTo,
        // cc: ["kjimenezdev@gmail.com", "roldanmanfred93@gmail.com", "earb23@gmail.com"],
        subject: 'Formulario de Contacto',
        text: 'Test'
    };
    var transporter = nodemailer.createTransport({
        host: options.host || 'smtp.ucr.ac.cr', // UCR SMTP Server
        port: options.port || 465,     // secure SMTP
        secure: options.secure || true, // false for TLS - as a boolean not string - but the default is false so just remove this completely
        auth: options.auth,
        tls: options.tls || {ciphers: 'SSLv3'}
    });
    transporter.sendMail({
        from: options.from,
        to: options.to,
        subject: options.subject,
        cc: options.cc,
        bcc: options.bcc,
        text: options.text,
        html: options.html,
        attachments: options.attachments
    }, function (err, info) {
        if (err) {
            // console.log('Error: ' + err);
            res.status(420);
            res.statusMessage = "Insuficient weed";
            res.json({
                error: err
            });
        }
        else {
            res.status(201);
            res.statusMessage = "OK";
            res.json({
                content: info
            });

        }
    });
});


/**
 * Sends a mail to the mail address received from param
 */
router.post('/contact', function (req, res) {
    req.checkBody({
        'name': {
            notEmpty: true,
            errorMessage: "Invalid name"
        }
    });
    req.checkBody({
        'subject': {
            notEmpty: true,
            errorMessage: "Invalid subject"
        }
    });
    req.checkBody({
        'email': {
            notEmpty: true,
            errorMessage: "Invalid email"
        }
    });
    req.checkBody({
        'message': {
            notEmpty: true,
            errorMessage: "Invalid message"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.statusMessage = "Insuficient weed";
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }

        var filePath = path.join(__dirname, 'mail-templates/contact.html');
        console.log(filePath);
        console.log(__dirname);

        fs.readFile(filePath, {encoding: 'utf-8'}, function (err, data) {
            if (!err) {

                console.log('received data: ' + data);
                const email = req.body.email;
                const subject = req.body.subject;
                const message = req.body.message;
                const name = req.body.name;
                const mailContent = data.replace('$SUBJECT', subject)
                    .replace('$NAME', name).replace('$MAIL', email)
                    .replace('$MESSAGE', message);

                const options = {
                    auth: {
                        user: "admin.piosa@ucr.ac.cr",
                        pass: "hGfbgfrx"
                    }, from: "admin.piosa@ucr.ac.cr",
                    to: "kjimenezdev@gmail.com",//teodoro.willinkcastro@ucr.ac.cr
                    // cc: ["piosaevents@gmail.com"],
                    subject: subject,
                    html: mailContent
                };

                var transporter = nodemailer.createTransport({
                    host: options.host || 'smtp.ucr.ac.cr', // UCR SMTP Server
                    port: options.port || 465,     // secure SMTP
                    secure: options.secure || true, // false for TLS - as a boolean not string - but the default is false so just remove this completely
                    auth: options.auth,
                    tls: options.tls || {ciphers: 'SSLv3'}
                });
                transporter.sendMail({
                    from: options.from,
                    to: options.to,
                    subject: options.subject,
                    cc: options.cc,
                    bcc: options.bcc,
                    text: options.text,
                    html: options.html,
                    attachments: options.attachments

                }, function (err, info) {
                    if (err) {
                        // console.log('Error: ' + err);
                        res.status(420);
                        res.statusMessage = "Insuficient weed";
                        res.json({
                            error: err
                        });
                    }
                    else {
                        res.status(200);
                        res.statusMessage = "OK";
                        res.json({
                            content: info
                        });

                    }
                });

            } else {
                res.status(400);
                res.statusMessage = "ERROR";
                res.json({
                    error: err
                });
                console.log(err);
            }
        });

    });
});


module.exports = router;
