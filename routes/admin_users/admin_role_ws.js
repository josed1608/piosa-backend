// Admin Role webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var AdminRole = require('../../entity/admin_users/admin_role');
var AdminUser = require('../../entity/admin_users/admin_user');
var RoleType = require('../../entity/admin_users/role_type');
var Entity = require('../../entity/admin_users/entity');
require('../../util/date-utils');

/**
 * Retrieves all the Admin Roles
 */
router.get('/findAll', function (req, res) {
    AdminRole.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No Admin Roles found'});
        }
    });
});

/**
 *  Retrieves all the objects matching the id received as param
 * @param {id} input the Admin Role's id
 */

router.get('/findById/:id', function (req, res, next) {
    if (req.params.id) {
        AdminRole.getById(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No AdminRole found'});
        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 *  Retrieves all the objects matching the id received as param
 * @param {id} input the Admin Role's id
 */
router.get('/findByAdmin/:id', function (req, res, next) {
    if (req.params.id) {
        AdminRole.getByAdminId(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No AdminRole found'});
        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 *  Retrieves all the objects matching the id received as param
 * @param {id} input the Admin Role's id
 */
router.get('/findDataByAdmin/:id', function (req, res, next) {
    if (req.params.id) {
        AdminRole.getDataByAdminId(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No AdminRole found'});
        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 * Creates a object from the provided params
 * @param {admin_id} the Admin Role's id
 * @param {entity_id} the Entity
 * @param {role_type} the Role Type
 */
router.post('/create', function (req, res) {
    req.checkBody({
        'admin_id': {
            notEmpty: true,
            errorMessage: "Invalid admin_id"
        }
    });
    req.checkBody({
        'entity_id': {
            notEmpty: true,
            errorMessage: "Invalid admin_id"
        }
    });
    req.checkBody({
        'role_type': {
            notEmpty: true,
            errorMessage: "Invalid role_type"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }

        // Validates existent user
        var adminId = req.body.admin_id;
        AdminRole.getByAdminAndEntity(req.body, function (err, rows) {
            if (err)
                res.json(err);
            if (rows.length) {
                res.status(410);
                res.json({message: 'User already has role'});

            } else {

                console.log('1');
                AdminUser.getById(adminId, function (err, rows) {
                    if (err)
                        res.json(err);

                    if (!rows.length) {
                        res.status(411);
                        res.json({message: 'Admin user does not exist!!'});

                    } else {
                        console.log('2');
                        // Validates existent entity
                        var entityId = req.body.entity_id;
                        Entity.getById(entityId, function (err, rows) {
                            if (err)
                                res.json(err);

                            if (!rows.length) {
                                res.status(412);
                                res.json({message: 'Entity does not exist!!'});

                            } else {
                                console.log('3');
                                var roleType = req.body.role_type;
                                RoleType.getById(roleType, function (err, rows) {
                                    if (err)
                                        res.json(err);
                                    if (!rows.length) {
                                        res.status(413);
                                        res.json({message: 'Role type does not exist!!'});
                                    } else {

                                        console.log('4');
                                        AdminRole.create(req.body, function (err, rows) {
                                            if (err)
                                                res.send(err);
                                            console.log(rows);
                                            if (rows != null) {
                                                console.log(rows);
                                                res.json(rows);
                                            } else {
                                                res.status(414);
                                                res.json({message: 'Db error!!'});
                                            }
                                        });

                                    }
                                });

                            }
                        });
                    }
                });
            }
        });
    });
});

/**
 * Updates an object by its id
 * @param {AdminRole_id} input the AdminRole's id
 */
router.put('/update', function (req, res) {
    req.checkBody({
        'admin_id': {
            notEmpty: true,
            errorMessage: "Invalid admin_id"
        }
    });
    req.checkBody({
        'entity_id': {
            notEmpty: true,
            errorMessage: "Invalid entity_id"
        }
    });
    req.checkBody({
        'role_type': {
            notEmpty: true,
            errorMessage: "Invalid role_type"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }

        AdminRole.updateRoleType(req.body, function (err, adminRole) {
            if (err)
                res.send(err);
            if (adminRole) {
                res.json(adminRole);
            }
        });
    });
});

/**
 * Deletes an object by its id
 * @param {req.body.admin_id} the Admin's id
 * @param {req.body.entity_id} the Entity's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.admin_id && req.body.entity_id) {
        AdminRole.delete(req.body, function (err, rows) {
            if (err)
                res.json(err);
            res.json(rows);
        });
    } else {
        res.send({error: 'No rows'});
    }
});


module.exports = router;
