// Admin Log Web Service

var express = require('express');
var router = express.Router();
var AdminLog = require('../../entity/admin_users/admin_log');
require('../../util/date-utils');


/**
 * Find Admin Log by id
 */
router.get('/findById/:id', function (req, res) {
    if (req.params.id) {
        AdminLog.getById(req.params.id, function (err, rows) {
            if (err)
                res.json(err);
            res.json(rows);

        });
    } else {
        res.send({error: 'No rows'});
    }
});

/**
 * Find all Admin Logs
 */
router.get('/findAll', function (req, res) {
    AdminLog.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No data'});
        }
    });
});

/**
 * Deletes an object by its id
 * @param {id} the object's id
 */
// router.delete('/delete', function (req, res) {
//     if (req.body.id) {
//         AdminLog.deleteById(req.body.id, function (err, rows) {
//             if (err)
//                 res.json(err);
//             res.json(rows);
//         });
//     } else {
//         res.send({error: 'No rows'});
//     }
// });


module.exports = router;
