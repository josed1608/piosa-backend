// AdminType webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var AdminType = require('../../entity/admin_users/admin_type');
require('../../util/date-utils');

/**
 * Retrieves all the AdminTypes currently stored on mongo db
 */
router.get('/findAll', function (req, res, next) {
    AdminType.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No AdminTypes found'});
        }
    });
});

/**
 *  Retrieves all the objects matching the id received as param
 * @param {id} input the Admin User's id
 */

router.get('/findById/:id', function (req, res, next) {
    if (req.params.id) {
        AdminType.getById(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No AdminType found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});


module.exports = router;
