// AdminUser webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();

var AdminUser = require('../../entity/admin_users/admin_user');
var AdminToken = require('../../entity/admin_users/admin_token');
var AdminRole = require('../../entity/admin_users/admin_role');

var nodemailer = require('nodemailer');
var bcrypt = require('bcryptjs');
var fs = require('fs');
var path = require('path');


var BCRYPT_SALT_ROUNDS = 12;
require('../../util/date-utils');

/**
 * Retrieves all the Admin Users
 */
router.get('/findAll', function (req, res, next) {
    AdminUser.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No AdminUsers found'});
        }
    });
});


/**
 * Retrieves all active Admin Users
 */
router.get('/findAll', function (req, res, next) {
    AdminUser.getActive(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No AdminUsers found'});
        }
    });
});

/**
 *  Retrieves all the objects matching the id received as param
 * @param {id} input the Admin User's id
 */
router.get('/findById/:id', function (req, res, next) {
    if (req.params.id) {
        AdminUser.getById(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No AdminUser found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 * Creates a object from the provided params
 * @param {name} input the Admin User's name
 * @param {email} input the Admin User's email
 * @param {password} input the Admin User's password
 */
// router.post('/register', function (req, res) {
//     req.checkBody({
//         'name': {
//             notEmpty: true,
//             errorMessage: "Invalid name"
//         }
//     });
//     req.checkBody({
//         'email': {
//             notEmpty: true,
//             errorMessage: "Invalid email"
//         }
//     });
//     // req.checkBody({
//     //     'password': {
//     //         notEmpty: true,
//     //         errorMessage: "Invalid password"
//     //     }
//     // });
//     req.getValidationResult().then(function (result) {
//         if (!result.isEmpty()) {
//             res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
//             return;
//         }
//
//         var email = req.body.email;
//         AdminUser.getByEmail(email, function (err, rows) {
//             if (err)
//                 res.json(err);
//
//             if (rows.length) {
//                 res.status(420);
//                 res.json({message: 'Mail already in use!'});
//
//             } else {
//                 var newAdminUser = req.body;
//                 newAdminUser.creation_date = new Date().toMysqlFormat();
//                 bcrypt.hash(req.body.password, BCRYPT_SALT_ROUNDS, function (err, hash) {
//
//                     newAdminUser.password = hash;
//                     AdminUser.register(newAdminUser, function (err, rows) {
//                         if (err)
//                             res.send(err);
//                         if (rows != null) {
//                             console.log(rows);
//
//                             res.json(rows);
//                         }
//                     });
//
//                 });
//
//             }
//         });
//
//
//     });
// });


router.post('/create', function (req, res) {
    req.checkBody({
        'name': {
            notEmpty: true,
            errorMessage: "Invalid name"
        }
    });
    req.checkBody({
        'email': {
            notEmpty: true,
            errorMessage: "Invalid email"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }

        var name = req.body.name;
        var email = req.body.email;
        AdminUser.getByEmail(email, function (err, rows) {
            if (err)
                res.json(err);

            if (rows.length) {
                res.status(420);
                res.json({message: 'Mail already in use!'});

            } else {

                AdminUser.create(req.body, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        if (rows.length > 0) {

                            // Crea el token para confirmar el usuario
                            var id = rows[0].id;
                            var exp = new Date().toMysqlFormat();
                            console.log(exp);
                            bcrypt.hash(req.body.password, BCRYPT_SALT_ROUNDS, function (err, hash) {

                                AdminToken.create(id, hash, exp, false, function (err, rows) {
                                    if (err)
                                        res.send(err);
                                    if (rows != null) {
                                        if (rows.length > 0) {

                                            // Crea el token para confirmar el usuario
                                            var filePath = path.join(__dirname, '../util/mail-templates/confirm_account.html');
                                            console.log(filePath);
                                            console.log(__dirname);

                                            fs.readFile(filePath, {encoding: 'utf-8'}, function (err, data) {
                                                if (!err) {

                                                    console.log('received data: ' + data);
                                                    const mailContent = data.replace('$USER_NAME', name).replace('$CONFIRM_TOKEN', hash);

                                                    const options = {
                                                        auth: {
                                                            user: "admin.piosa@ucr.ac.cr",
                                                            pass: "hGfbgfrx"
                                                        }, from: "admin.piosa@ucr.ac.cr",
                                                        to: email,
                                                        subject: "Confirmación de cuenta",
                                                        html: mailContent
                                                    };

                                                    var transporter = nodemailer.createTransport({
                                                        host: options.host || 'smtp.ucr.ac.cr', // UCR SMTP Server
                                                        port: options.port || 465,     // secure SMTP
                                                        secure: options.secure || true, // false for TLS - as a boolean not string - but the default is false so just remove this completely
                                                        auth: options.auth,
                                                        tls: options.tls || {ciphers: 'SSLv3'}
                                                    });

                                                    transporter.sendMail({
                                                        from: options.from,
                                                        to: options.to,
                                                        subject: options.subject,
                                                        cc: options.cc,
                                                        bcc: options.bcc,
                                                        text: options.text,
                                                        html: options.html,
                                                        attachments: options.attachments

                                                    }, function (err, info) {
                                                        if (err) {
                                                            // console.log('Error: ' + err);
                                                            res.status(420);
                                                            res.statusMessage = "Insuficient weed";
                                                            res.json({
                                                                error: err
                                                            });
                                                        }
                                                        else {
                                                            res.status(200);
                                                            res.statusMessage = "OK";
                                                            res.json({
                                                                content: info
                                                            });

                                                        }
                                                    });

                                                } else {
                                                    res.status(400);
                                                    res.statusMessage = "ERROR";
                                                    res.json({
                                                        error: err
                                                    });
                                                    console.log(err);
                                                }
                                            });
                                        }
                                    }
                                });

                            });
                        }
                    }
                });
            }
        });


    });
});

router.post('/confirm', function (req, res) {
    req.checkBody({
        'admin_id': {
            notEmpty: true,
            errorMessage: "Invalid admin_id"
        }
    });
    req.checkBody({
        'token': {
            notEmpty: true,
            errorMessage: "Invalid token"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }

        var admin_id = req.body.admin_id;
        var token = req.body.token;
        AdminToken.get(admin_id, token, function (err, rows) {
            if (err)
                res.json(err);

            if (rows.length) {
                AdminToken.use(true, admin_id, token, function (err, rows) {
                    if (err)
                        res.json(err);

                    res.status(200);
                    res.json({
                        ok: 'confirmed'
                    });
                });

            } else {
                res.status(420);
                res.json({message: 'Error with confirmation!'});

            }
        });


    });
});


/**
 * Creates a object from the provided params
 * @param {id} input the Admin User's id
 * @param {password} input the Admin User's password
 */
router.post('/login', function (req, res) {
    req.checkBody({
        'email': {
            notEmpty: true,
            errorMessage: "Invalid email"
        }
    });
    req.checkBody({
        'password': {
            notEmpty: true,
            errorMessage: "Invalid password"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var email = req.body.email;
        AdminUser.getByEmail(email, function (err, rows) {
            if (err) {
                res.status(420);
                console.error(err);
                res.statusMessage = "Not found";
                res.send(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {

                    var adminFound = rows[0];
                    var password = req.body.password;
                    var adminFoundPassword = adminFound.password;
                    bcrypt.compare(password, adminFoundPassword, function (err, isMatch) {
                        if (isMatch) {
                            res.status(200);
                            res.statusMessage = "OK";
                            const object = adminFound;
                            object.password = undefined;
                            res.json(object);
                        } else {
                            res.status(301);
                            res.statusMessage = "No data";
                            res.send({error: 'Incorrect password'});
                        }
                    });

                } else {
                    res.status(300);
                    res.statusMessage = "No data";
                    res.send({error: 'No Admin User found'});
                }
            }
        });
    });
});

/**
 * Creates a object from the provided params
 * @param {email} input the Admin User's password
 */
router.post('/resetPassword', function (req, res) {
    req.checkBody({
        'email': {
            notEmpty: true,
            errorMessage: "Invalid email"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var email = req.body.email;
        AdminUser.getByEmail(email, function (err, rows) {
            if (err) {
                res.status(420);
                console.error(err);
                res.statusMessage = "Not found";
                res.send(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {

                    var adminFound = rows[0];
                    const name = adminFound.name;

                    var filePath = path.join(__dirname, '../util/mail-templates/reset_password.html');
                    console.log(filePath);
                    console.log(__dirname);

                    fs.readFile(filePath, {encoding: 'utf-8'}, function (err, data) {
                        if (!err) {

                            console.log('received data: ' + data);
                            const mailContent = data.replace('$USER_NAME', name);

                            const options = {
                                auth: {
                                    user: "admin.piosa@ucr.ac.cr",
                                    pass: "hGfbgfrx"
                                }, from: "admin.piosa@ucr.ac.cr",
                                to: adminFound.email,
                                subject: "Restauración de contraseña",
                                html: mailContent
                            };

                            var transporter = nodemailer.createTransport({
                                host: options.host || 'smtp.ucr.ac.cr', // UCR SMTP Server
                                port: options.port || 465,     // secure SMTP
                                secure: options.secure || true, // false for TLS - as a boolean not string - but the default is false so just remove this completely
                                auth: options.auth,
                                tls: options.tls || {ciphers: 'SSLv3'}
                            });
                            transporter.sendMail({
                                from: options.from,
                                to: options.to,
                                subject: options.subject,
                                cc: options.cc,
                                bcc: options.bcc,
                                text: options.text,
                                html: options.html,
                                attachments: options.attachments

                            }, function (err, info) {
                                if (err) {
                                    // console.log('Error: ' + err);
                                    res.status(420);
                                    res.statusMessage = "Insuficient weed";
                                    res.json({
                                        error: err
                                    });
                                }
                                else {
                                    res.status(200);
                                    res.statusMessage = "OK";
                                    res.json({
                                        content: info
                                    });

                                }
                            });

                        } else {
                            res.status(400);
                            res.statusMessage = "ERROR";
                            res.json({
                                error: err
                            });
                            console.log(err);
                        }
                    });


                } else {
                    res.status(300);
                    res.statusMessage = "No data";
                    res.send({error: 'No Admin User found'});
                }
            }
        });
    });
});


/**
 * Deletes an object by its id
 * @param {req.body.id} the object's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.id) {
        AdminUser.delete(req.body.id, function (err, rows) {
            if (err)
                res.json(err);
            res.json(rows);
        });
    } else {
        res.send({error: 'No rows'});
    }
});


module.exports = router;
