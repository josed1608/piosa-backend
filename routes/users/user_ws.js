// User webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var User = require('../../entity/users/user');
var bcrypt = require('bcryptjs');

var BCRYPT_SALT_ROUNDS = 12;
require('../../util/date-utils');

/**
 * Retrieves all the users currently stored on mongo db
 */
router.get('/findAll', function (req, res) {
    User.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Error";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No users found'});
        }
    });
});

/**
 *  Retrieves all the objects matching the id received as param
 * @param {userId} input the user's id
 */

router.get('/findById/:id', function (req, res) {
    if (req.params.id) {
        User.getById(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Error";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No user found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Error";
        res.json(err);
    }
});

/**
 * Creates a object from the provided params
 * @param {email} input the user's email
 * @param {password} input the user's password
 */
router.post('/create', function (req, res) {
    req.checkBody({
        'name': {
            notEmpty: true,
            errorMessage: "Invalid name"
        }
    });
    req.checkBody({
        'email': {
            notEmpty: true,
            errorMessage: "Invalid email"
        }
    });
    req.checkBody({
        'password': {
            notEmpty: true,
            errorMessage: "Invalid password"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var email = req.body.email;
        User.getByEmail(email, function (err, rows) {
            if (err)
                res.json(err);

            if (rows.length) {
                res.status(420);
                res.json({message: 'Mail already in use!'});

            } else {

                var newUser = req.body;
                newUser.creation_date = new Date().toMysqlFormat();
                bcrypt.hash(req.body.password, BCRYPT_SALT_ROUNDS, function (err, hash) {
                    newUser.password = hash;
                    User.create(newUser, function (err, rows) {
                        if (err)
                            res.send(err);
                        if (rows != null) {
                            console.log(rows);
                            res.json(rows);
                        }
                    });

                });
            }
        });

    });
});

/**
 * Creates a object from the provided params
 * @param {id} input the user's id
 * @param {password} input the user's password
 */
router.post('/login', function (req, res) {
    req.checkBody({
        'id': {
            notEmpty: true,
            errorMessage: "Invalid id"
        }
    });
    req.checkBody({
        'password': {
            notEmpty: true,
            errorMessage: "Invalid password"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var id = req.body.id;
        User.getById(id, function (err, rows) {
            if (err) {
                res.status(420);
                console.error(err);
                res.statusMessage = "Error";
                res.send(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {

                    var userFound = rows[0];
                    var password = req.body.password;

                    if (bcrypt.compare(userFound.password, password)) {
                        res.status(200);
                        res.statusMessage = "OK";
                        const object = userFound;
                        object.password = undefined;
                        res.json(object);
                        return;
                    } else {

                        res.status(301);
                        res.statusMessage = "No data";
                        res.send({error: 'Incorrect password'});
                    }
                }
                res.status(300);
                res.statusMessage = "No data";
                res.send({error: 'No user found'});
            }
        });
    });
});

/**
 * Updates an object by its id
 * @param {user_id} input the user's id
 */
// router.put('/update', function (req, res) {
//     req.checkBody({
//         'user_id': {
//             notEmpty: true,
//             errorMessage: "Invalid user_id"
//         }
//     });
//     req.checkBody({
//         'name': {
//             notEmpty: true,
//             errorMessage: "Invalid name"
//         }
//     });
//     req.checkBody({
//         'email': {
//             notEmpty: true,
//             errorMessage: "Invalid email"
//         }
//     });
//     req.checkBody({
//         'password': {
//             notEmpty: true,
//             errorMessage: "Invalid password"
//         }
//     });
//     req.getValidationResult().then(function (result) {
//         if (!result.isEmpty()) {
//             res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
//             return;
//         }
//
//         User.findById(req.body.objectId, function (err, user) {
//             if (err)
//                 res.send(err);
//             if (user) {
//                 user.email = req.body.email;
//                 user.password = req.body.password;
//                 user.save(function (err) {
//                     if (err) {
//                         res.send(err);
//                     }
//                     res.json({
//                         content: user
//                     });
//                 });
//             }
//         });
//     });
// });

/**
 * Deletes an object by its id
 * @param {req.body.id} the object's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.id) {
        User.delete(req.body.id, function (err, rows) {
            if (err)
                res.json(err);
            res.json(rows);
        });
    } else {
        res.send({error: 'No rows'});
    }
});


module.exports = router;
