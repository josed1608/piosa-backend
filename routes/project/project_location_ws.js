// ProjectLocation webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var ProjectLocation = require('../../entity/project/project_location');
var Project = require('../../entity/project/project');

/**
 * Retrieves all the users
 */
router.get('/findAll', function (req, res) {
    ProjectLocation.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No advances found'});
        }
    });
});

/**
 *  Retrieves all the objects matching the id received as param
 * @param {userId} input the user's id
 */
router.get('/findById/:id', function (req, res) {
    if (req.params.id) {
        ProjectLocation.getById(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'Not found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 *
 */
router.get('/findByProject/:id', function (req, res) {
    if (req.params.id) {
        ProjectLocation.getByProject(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'Not found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 * Creates a object from the provided params
 * @param {name} input the user's name
 */
router.post('/create', function (req, res) {
    req.checkBody({
        'project_id': {
            notEmpty: true,
            errorMessage: "Invalid project_id"
        }
    });
    req.checkBody({
        'latitude': {
            notEmpty: true,
            errorMessage: "Invalid latitude"
        }
    });
    req.checkBody({
        'longitude': {
            notEmpty: true,
            errorMessage: "Invalid longitude"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var project_id = req.body.project_id;
        Project.getById(project_id, function (err, rows) {
            if (err)
                res.json(err);

            if (rows.length) {
                ProjectLocation.create(req.body, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        res.json(rows);
                    }
                });
            } else {
                res.status(420);
                res.json({message: 'ProjectLocation already exists!'});
            }
        });
    });
});

/**
 *
 */
router.put('/update', function (req, res) {
    req.checkBody({
        'id': {
            notEmpty: true,
            errorMessage: "Invalid id"
        }
    });
    req.checkBody({
        'project_id': {
            notEmpty: true,
            errorMessage: "Invalid project_id"
        }
    });
    req.checkBody({
        'latitude': {
            notEmpty: true,
            errorMessage: "Invalid latitude"
        }
    });
    req.checkBody({
        'longitude': {
            notEmpty: true,
            errorMessage: "Invalid longitude"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var id = req.body.id;
        ProjectLocation.getById(id, function (err, rows) {
            if (err)
                res.json(err);

            if (!rows.length) {
                res.status(420);
                res.json({message: 'ProjectLocation already exists!'});
            } else {

                ProjectLocation.update(req.body, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        res.json(rows);
                    }
                });
            }
        });
    });
});

/**
 * Deletes an object by its id
 * @param {req.body.id} the object's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.id) {
        ProjectLocation.delete(req.body.id, function (err, rows) {
            if (err) {
                res.json(err);
            }

            res.json(rows);
        });
    } else {
        res.send({error: 'No rows'});
    }
});


module.exports = router;
