// ProjectCollaborator webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var ProjectCollaborator = require('../../entity/project/project_collaborator');
var Project = require('../../entity/project/project');

/**
 * Retrieves all the project collaborators
 */
router.get('/findAll', function (req, res) {
    ProjectCollaborator.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No data found'});
        }
    });
});

/**
 * Retrieves all data by project collaborator
 */
router.get('/findDataByProject/:id', function (req, res) {
    if (req.params.id) {
        ProjectCollaborator.getDataByProject(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
            }
            if (rows.length) {
                res.status(200);
                res.statusMessage = "OK";
                res.json(rows);
            }
            else {
                res.status(300);
                res.statusMessage = "No data";
                res.send({error: 'No data found'});
            }
        });
    } else {
        res.status(400);
        res.statusMessage = "No id";
        res.send({error: 'No id'});
    }
});


router.get('/findDataByCollaborator/:id', function (req, res) {
    if (req.params.id) {
        ProjectCollaborator.getDataByCollaborator(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
            }
            if (rows.length) {
                res.status(200);
                res.statusMessage = "OK";
                res.json(rows);
            }
            else {
                res.status(300);
                res.statusMessage = "No data";
                res.send({error: 'No entities found'});
            }
        });
    } else {
        res.status(400);
        res.statusMessage = "No id";
        res.send({error: 'No id'});
    }
});


/**
 * Creates a object from the provided params
 * @param {project_id} input the user's project_id
 * @param {collaborator_id} input the user's collaborator_id
 */
router.post('/create', function (req, res) {
    req.checkBody({
        'project_id': {
            notEmpty: true,
            errorMessage: "Invalid project_id"
        }
    });
    req.checkBody({
        'collaborator_id': {
            notEmpty: true,
            errorMessage: "Invalid collaborator_id"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var id = req.body.project_id;
        Project.getById(id, function (err, rows) {
            if (err)
                res.json(err);

            if (rows.length) {

                var collaborator_id = req.body.collaborator_id;
                ProjectCollaborator.getByProjAndCollaborator(id, collaborator_id, function (err, rows) {
                    if (err)
                        res.json(err);

                    if (!rows.length) {

                        ProjectCollaborator.create(req.body, function (err, rows) {
                            if (err)
                                res.send(err);
                            if (rows != null) {
                                res.json(rows);
                            }
                        });
                    } else {

                        res.status(422);
                        res.json({message: 'Proyect not exists!'});
                    }
                });

            } else {
                res.status(421);
                res.json({message: 'Proyect not exists!'});
            }
        });
    });
});

/**
 * Deletes an object by its id
 * @param {req.body.id} the object's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.project_id != null && req.body.collaborator_id != null) {
        ProjectCollaborator.delete(req.body.project_id, req.body.collaborator_id, function (err, rows) {
            if (err) {
                res.status(400);
                res.json(err);
            }
            res.status(200);
            res.json(rows);
        });
    } else {
        res.status(300);
        res.send({error: 'No rows'});
    }
});

module.exports = router;
