// ProjectCoordinator webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var ProjectCoordinator = require('../../entity/project/project_coordinator');
var Project = require('../../entity/project/project');

/**
 * Retrieves all the coordinators
 */
router.get('/findAll', function (req, res) {
    ProjectCoordinator.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No data found'});
        }
    });
});


/**
 * Retrieves all data by project collaborator
 */
router.get('/findDataByProject/:id', function (req, res) {
    if (req.params.id) {
        ProjectCoordinator.getDataByProject(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
            }
            if (rows.length) {
                res.status(200);
                res.statusMessage = "OK";
                res.json(rows);
            }
            else {
                res.status(300);
                res.statusMessage = "No data";
                res.send({error: 'No data found'});
            }
        });
    } else {
        res.status(400);
        res.statusMessage = "No id";
        res.send({error: 'No id'});
    }
});


router.get('/findDataByCoordinator/:id', function (req, res) {
    if (req.params.id) {
        ProjectCoordinator.getDataByCollaborator(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
            }
            if (rows.length) {
                res.status(200);
                res.statusMessage = "OK";
                res.json(rows);
            }
            else {
                res.status(300);
                res.statusMessage = "No data";
                res.send({error: 'No entities found'});
            }
        });
    } else {
        res.status(400);
        res.statusMessage = "No id";
        res.send({error: 'No id'});
    }
});


/**
 * Creates a object from the provided params
 * @param {name} input the user's name
 */
router.post('/create', function (req, res) {
    req.checkBody({
        'project_id': {
            notEmpty: true,
            errorMessage: "Invalid project_id"
        }
    });
    req.checkBody({
        'coordinator_id': {
            notEmpty: true,
            errorMessage: "Invalid coordinator_id"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var id = req.body.project_id;
        Project.getById(id, function (err, rows) {
            if (err)
                res.json(err);

            if (rows.length) {

                var coordinator = req.body.coordinator_id;
                ProjectCoordinator.getByProjAndCoordinator(id, coordinator, function (err, rows) {
                    if (err)
                        res.json(err);
                    if (!rows.length) {

                        ProjectCoordinator.create(req.body, function (err, rows) {
                            if (err)
                                res.send(err);
                            if (rows != null) {
                                res.json(rows);
                            }
                        });
                    } else {

                        res.status(422);
                        res.json({message: 'already existent!'});
                    }
                });

            } else {
                res.status(421);
                res.json({message: 'Proyect not exists!'});
            }
        });
    });
});


/**
 * Deletes an object by its id
 * @param {req.body.id} the object's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.project_id != null && req.body.coordinator_id != null) {
        ProjectCoordinator.delete(req.body.project_id, req.body.coordinator_id, function (err, rows) {
            if (err) {
                res.status(400);
                res.json(err);
            }
            res.status(200);
            res.json(rows);
        });
    } else {
        res.status(300);
        res.send({error: 'No rows'});
    }
});


module.exports = router;
