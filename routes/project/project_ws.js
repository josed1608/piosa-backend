// Proyect webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var Proyect = require('../../entity/project/project');

/**
 * Retrieves all the proyects
 */
router.get('/findAll', function (req, res) {
    Proyect.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No entities found'});
        }
    });
});

/**
 * Retrieves all the active proyects
 */
router.get('/findActive', function (req, res) {
    Proyect.getActive(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No entities found'});
        }
    });
});

/**
 *  Retrieves all the objects matching the id received as param
 * @param {userId} input the user's id
 */
router.get('/findById/:id', function (req, res) {
    if (req.params.id) {
        Proyect.getById(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No entity found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 *
 */
router.get('/findByType/:type', function (req, res) {
    if (req.params.type) {
        Proyect.getByType(req.params.type, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No entity found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

router.get('/findByProfessor/:id', function (req, res) {
    if (req.params.id) {
        Proyect.getByProfessor(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No entity found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

router.get('/findByCollaborator/:id', function (req, res) {
    if (req.params.id) {
        Proyect.getByCollaborator(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No entity found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 *
 */
router.get('/findActiveByType/:type', function (req, res) {
    if (req.params.type) {
        Proyect.getActiveByType(req.params.type, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No entity found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 * Creates a object from the provided params
 * @param {name} input the user's name
 */
router.post('/create', function (req, res) {
    req.checkBody({
        'id': {
            notEmpty: true,
            errorMessage: "Invalid id"
        }
    });
    req.checkBody({
        'name': {
            notEmpty: true,
            errorMessage: "Invalid name"
        }
    });
    req.checkBody({
        'type': {
            notEmpty: true,
            errorMessage: "Invalid type"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var id = req.body.id;
        Proyect.getById(id, function (err, rows) {
            if (err)
                res.json(err);

            if (rows.length) {
                res.status(420);
                res.json({message: 'Proyect already exists!'});
            } else {

                var object = req.body;
                object.creation_date = new Date().toMysqlFormat();
                Proyect.create(object, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        res.json(rows);
                    }
                });
            }
        });
    });
});

/**
 *
 */
router.put('/update', function (req, res) {
    req.checkBody({
        'id': {
            notEmpty: true,
            errorMessage: "Invalid id"
        }
    });
    req.checkBody({
        'name': {
            notEmpty: true,
            errorMessage: "Invalid name"
        }
    });
    req.checkBody({
        'type': {
            notEmpty: true,
            errorMessage: "Invalid type"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var id = req.body.id;
        Proyect.getById(id, function (err, rows) {
            if (err)
                res.json(err);

            if (!rows.length) {
                res.status(420);
                res.json({message: 'Proyect already exists!'});
            } else {
                var object = req.body;
                object.modif_date = new Date().toMysqlFormat();
                Proyect.update(object, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        res.json(rows);
                    }
                });
            }
        });
    });
});


/**
 * Deletes an object by its id
 * @param {req.body.id} the object's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.id) {
        Proyect.delete(req.body.id, function (err, rows) {
            if (err)
                res.json(err);
            res.json(rows);
        });
    } else {
        res.send({error: 'No rows'});
    }
});


module.exports = router;
