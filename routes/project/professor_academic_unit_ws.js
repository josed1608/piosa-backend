// ProfessorAcademicUnit webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var ProfessorAcademicUnit = require('../../entity/team_members/professor_academic_unit');
var Professor = require('../../entity/team_members/professor');

/**
 * Retrieves all the users
 */
router.get('/findAll', function (req, res) {
    ProfessorAcademicUnit.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No advances found'});
        }
    });
});


/**
 * Retrieves all data by project collaborator
 */
router.get('/findDataByProfessor/:id', function (req, res) {
    if (req.params.id) {
        ProfessorAcademicUnit.getDataByProfessor(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
            }
            if (rows.length) {
                res.status(200);
                res.statusMessage = "OK";
                res.json(rows);
            }
            else {
                res.status(300);
                res.statusMessage = "No data";
                res.send({error: 'No data found'});
            }
        });
    } else {
        res.status(400);
        res.statusMessage = "No id";
        res.send({error: 'No id'});
    }
});

/**
 * Creates a object from the provided params
 * @param {name} input the user's name
 */
router.post('/create', function (req, res) {
    req.checkBody({
        'unit_id': {
            notEmpty: true,
            errorMessage: "Invalid unit_id"
        }
    });
    req.checkBody({
        'professor_id': {
            notEmpty: true,
            errorMessage: "Invalid professor_id"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var id = req.body.professor_id;
        Professor.getById(id, function (err, rows) {
            if (err)
                res.json(err);

            if (rows.length) {
                ProfessorAcademicUnit.create(req.body, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        res.json(rows);
                    }
                });

            } else {
                res.status(421);
                res.json({message: 'Professor not exists!'});
            }
        });
    });
});


/**
 * Deletes an object by its id
 * @param {req.body.id} the object's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.professor_id != null&& req.body.unit_id != null) {
        ProfessorAcademicUnit.delete(req.body.professor_id, req.body.unit_id, function (err, rows) {
            if (err){
                res.status(400);
                res.json(err);
            }
            res.status(200);
            res.json(rows);
        });
    } else {
        res.status(300);
        res.send({error: 'No rows'});
    }
});


module.exports = router;
