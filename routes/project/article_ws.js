// Article webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var Article = require('../../entity/project/article');
var FacebookFeed = require('../../entity/project/facebook_feed');
var axios = require('axios');

/**
 * Retrieves all the articles
 */
router.get('/findAll', function (req, res) {
    Article.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No advances found'});
        }
    });
});


/**
 * Retrieves all the active articles
 */
router.get('/findActive/:n', function (req, res) {
    if (req.params.n) {
        Article.getActive(parseInt(req.params.n), function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
            }
            if (rows.length) {
                res.status(200);
                res.statusMessage = "OK";
                res.json(rows);
            }
            else {
                res.status(300);
                res.statusMessage = "No data";
                res.send({error: 'Not found'});
            }
        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "No n params";
        res.json(err);
    }
});

router.get('/facebookFeed', function (req, res) {
    FacebookFeed.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No data found'});
        }
    });
});

/**
 *  Retrieves all the objects matching the id received as param
 * @param {userId} input the user's id
 */
router.get('/findById/:id', function (req, res) {
    if (req.params.id) {
        Article.getById(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No entity found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 * Creates a object from the provided params
 * @param {name} input the user's name
 */
router.post('/create', function (req, res) {
    req.checkBody({
        'title': {
            notEmpty: true,
            errorMessage: "Invalid title"
        }
    });
    req.checkBody({
        'resume': {
            notEmpty: true,
            errorMessage: "Invalid resume"
        }
    });
    req.checkBody({
        'content': {
            notEmpty: true,
            errorMessage: "Invalid content"
        }
    });
    req.checkBody({
        'project_id': {
            notEmpty: true,
            errorMessage: "Invalid project id"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var title = req.body.title;
        Article.getByTitle(title, function (err, rows) {
            if (err)
                res.json(err);

            if (rows.length) {
                res.status(420);
                res.json({message: 'Article already exists!'});
            } else {
                var object = req.body;
                object.creation_date = new Date().toMysqlFormat();
                Article.create(object, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        res.json(rows);
                    }
                });
            }
        });
    });
});

/**
 *
 */
router.put('/update', function (req, res) {
    req.checkBody({
        'id': {
            notEmpty: true,
            errorMessage: "Invalid id"
        }
    });
    req.checkBody({
        'title': {
            notEmpty: true,
            errorMessage: "Invalid title"
        }
    });
    req.checkBody({
        'resume': {
            notEmpty: true,
            errorMessage: "Invalid resume"
        }
    });
    req.checkBody({
        'content': {
            notEmpty: true,
            errorMessage: "Invalid content"
        }
    });
    req.checkBody({
        'project_id': {
            notEmpty: true,
            errorMessage: "Invalid project id"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var id = req.body.id;
        Article.getById(id, function (err, rows) {
            if (err)
                res.json(err);

            if (!rows.length) {
                res.status(420);
                res.json({message: 'Article already exists!'});
            } else {
                var object = req.body;
                object.modif_date = new Date().toMysqlFormat();
                Article.update(object, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        res.json(rows);
                    }
                });
            }
        });
    });
});

/**
 * Updates all articles against Facebook
 */
router.get('/updateFacebookFeed', function (req, res) {
        axios.get(
            'https://graph.facebook.com/v2.8/1504078793169084/posts?access_token=267800490391854|1476ff670c7c2db25248737b5d3fc9a5&fields=id,message,link,full_picture,created_time')
            .then(function (response) {
                var data = response.data['data'];
                var data_arr = [];
                // console.log(data);
                for (var i = 0; i < data.length; i++) {
                    var object = data[i];
                    console.log(object);
                    var single_arr = [];
                    // for (var property in object) {
                    //                     //     if (object.hasOwnProperty(property)) {
                    //                     //         // if (object[property] == null) {
                    //                     //         //     single_arr.push('');
                    //                     //         // } else {
                    //                     //             single_arr.push(object[property]);
                    //                     //         // }
                    //                     //     } else {
                    //                     //         single_arr.push('');
                    //                     //     }
                    //                     //
                    //                     // }
                    var id = (object.id != null) ? object.id : '';
                    single_arr.push(id);
                    var message = (object.message != null) ? object.message : '';
                    single_arr.push(message);
                    var link = (object.link != null) ? object.link : '';
                    single_arr.push(link);
                    var full_picture = (object.full_picture != null) ? object.full_picture : '';
                    single_arr.push(full_picture);
                    var created_time = (object.created_time != null) ? object.created_time : '';
                    single_arr.push(created_time);

                    data_arr.push(single_arr);

                }
                console.log(data_arr);
                FacebookFeed.dropAll(function (err, datarr) {
                    FacebookFeed.bulkCreate(data_arr, function (err, rows) {
                        if (err) {
                            res.status(420);
                            console.error(err);
                            res.statusMessage = "Insuficient weed";
                            res.send(err);
                            return;
                        }
                        if (rows != null) {
                            if (rows.length) {
                                res.status(200);
                                res.statusMessage = "OK";
                                res.json(rows);
                                return;
                            }
                        }
                        res.status(300);
                        res.statusMessage = "No data";
                        res.send({error: 'No data found'});
                    });
                });


            })
            .catch(function (error) {
                console.log(error);
                res.json({
                    error: "ERROR updating facebook feed"
                });
            })
    }
);

/**
 * Deletes an object by its id
 * @param {req.body.id} the object's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.id) {
        Article.delete(req.body.id, function (err, rows) {
            if (err)
                res.json(err);
            res.json(rows);
        });
    } else {
        res.send({error: 'No rows'});
    }
});


module.exports = router;
