// ProjectTag webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var ProjectTag = require('../../entity/project/project_tag');

/**
 * Retrieves all the users
 */
router.get('/findAll', function (req, res) {
    ProjectTag.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'Not found'});
        }
    });
});


/**
 * Creates a object from the provided params
 * @param {name} input the user's name
 */
router.post('/create', function (req, res) {
    req.checkBody({
        'tag_id': {
            notEmpty: true,
            errorMessage: "Invalid title"
        }
    });
    req.checkBody({
        'description': {
            notEmpty: true,
            errorMessage: "Invalid description"
        }
    });
    req.checkBody({
        'proyect_id': {
            notEmpty: true,
            errorMessage: "Invalid project id"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var title = req.body.title;
        ProjectTag.getByTitle(title, function (err, rows) {
            if (err)
                res.json(err);

            if (rows.length) {
                res.status(420);
                res.json({message: 'ProjectTag already exists!'});
            } else {
                var object = req.body;
                object.creation_date = new Date().toMysqlFormat();
                ProjectTag.create(object, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        res.json(rows);
                    }
                });
            }
        });
    });
});

/**
 * Deletes an object by its id
 * @param {req.body.id} the object's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.id) {
        ProjectTag.delete(req.body.id, function (err, rows) {
            if (err)
                res.json(err);
            res.json(rows);
        });
    } else {
        res.send({error: 'No rows'});
    }
});


module.exports = router;
