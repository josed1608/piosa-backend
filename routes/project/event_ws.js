// Event webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var Event = require('../../entity/project/event');
const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');

// If modifying these scopes, delete credentials.json.
const SCOPES = ['https://www.googleapis.com/auth/calendar.readonly'];
const TOKEN_PATH = 'credentials.json';
var path = require('path');

/**
 * Retrieves all the events
 */
router.get('/findAll', function (req, res) {
    Event.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No events found'});
        }
    });
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 * @return {function} if error in reading credentials.json asks for a new one.
 */
function authorize(credentials, callback) {
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    let token = {};
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    try {
        token = fs.readFileSync(TOKEN_PATH);
    } catch (err) {
        return getAccessToken(oAuth2Client, callback);
    }
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client);
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getAccessToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
    oAuth2Client.getToken(code, (err, token) => {
        if (err) return callback(err);
    oAuth2Client.setCredentials(token);
    // Store the token to disk for later program executions
    try {
        fs.writeFileSync(TOKEN_PATH, JSON.stringify(token));
        console.log('Token stored to', TOKEN_PATH);
    } catch (err) {
        console.error(err);
    }
    callback(oAuth2Client);
});
});
}

/**
 * Lists the next 10 events on the user's primary calendar.
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function listEvents(auth) {
    const calendar = google.calendar({version: 'v3', auth});
    calendar.events.list({
        calendarId: 'primary',
        timeMin: (new Date()).toISOString(),
        maxResults: 10,
        singleEvents: true,
        orderBy: 'startTime',
    }, (err, res) => {
        if (err) return console.log('The API returned an error: ' + err);
    const events = res.data.items;
    if (events.length) {
        console.log('Upcoming 10 events:');
        events.map((event, i) => {
            const start = event.start.dateTime || event.start.date;
        console.log(`${start} - ${event.summary}`);
    });
    } else {
        console.log('No upcoming events found.');
    }
});
}
// [END calendar_quickstart]

router.get('/getFromGoogle', function (req, res) {

// Load client secrets from a local file.
    try {
        var filePath = path.join(__dirname, 'client_secret.json');
        console.log(filePath);
        // console.log(__dirname);
        const content = fs.readFileSync(filePath);
        authorize(JSON.parse(content), listEvents);
    } catch (err) {
        return console.log('Error loading client secret file:', err);
    }
});

/**
 * Retrieves all the active events
 */
router.get('/findActive', function (req, res) {
    Event.getActive(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No events found'});
        }
    });
});

/**
 *  Retrieves all the objects matching the id received as param
 * @param {userId} input the user's id
 */
router.get('/findById/:id', function (req, res) {
    if (req.params.id) {
        Event.getById(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No entity found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 * Creates a object from the provided params
 * @param {name} input the user's name
 */
router.post('/create', function (req, res) {
    req.checkBody({
        'title': {
            notEmpty: true,
            errorMessage: "Invalid title"
        }
    });
    req.checkBody({
        'description': {
            notEmpty: true,
            errorMessage: "Invalid description"
        }
    });
    req.checkBody({
        'project_id': {
            notEmpty: true,
            errorMessage: "Invalid project_id"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var object = req.body;
        object.creation_date = new Date().toMysqlFormat();
        Event.create(object, function (err, rows) {
            if (err)
                res.send(err);
            if (rows != null) {
                res.json(rows);
            }
        });
    });
});

/**
 *
 */
router.put('/update', function (req, res) {
    req.checkBody({
        'id': {
            notEmpty: true,
            errorMessage: "Invalid id"
        }
    });
    req.checkBody({
        'title': {
            notEmpty: true,
            errorMessage: "Invalid title"
        }
    });
    req.checkBody({
        'description': {
            notEmpty: true,
            errorMessage: "Invalid description"
        }
    });
    req.checkBody({
        'project_id': {
            notEmpty: true,
            errorMessage: "Invalid project_id"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var id = req.body.id;
        Event.getById(id, function (err, rows) {
            if (err)
                res.json(err);

            if (!rows.length) {
                res.status(420);
                res.json({message: 'Event already exists!'});
            } else {
                var object = req.body;
                object.modif_date = new Date().toMysqlFormat();
                Event.update(object, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        res.json(rows);
                    }
                });
            }
        });
    });
});

/**
 * Deletes an object by its id
 * @param {req.body.id} the object's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.id) {
        Event.delete(req.body.id, function (err, rows) {
            if (err)
                res.json(err);
            res.json(rows);
        });
    } else {
        res.send({error: 'No rows'});
    }
});


module.exports = router;
