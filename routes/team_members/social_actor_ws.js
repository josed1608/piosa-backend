// SocialActor webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var SocialActor = require('../../entity/team_members/social_actor');

/**
 * Retrieves all the social actors
 */
router.get('/findAll', function (req, res) {
    SocialActor.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No data found'});
        }
    });
});

/**
 * Retrieves all the active social actors
 */
router.get('/findActive', function (req, res) {
    SocialActor.getActive(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No data found'});
        }
    });
});

/**
 *  Retrieves all the objects matching the id received as param
 * @param {userId} input the user's id
 */
router.get('/findById/:id', function (req, res) {
    if (req.params.id) {
        SocialActor.getById(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No data found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 * Creates a object from the provided params
 * @param {name} input the user's name
 */
router.post('/create', function (req, res) {
    req.checkBody({
        'name': {
            notEmpty: true,
            errorMessage: "Invalid name"
        }
    });
    req.checkBody({
        'full_name': {
            notEmpty: true,
            errorMessage: "Invalid full_name"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var object = req.body;
        object.creation_date = new Date().toMysqlFormat();
        SocialActor.create(object, function (err, rows) {
            if (err)
                res.send(err);
            if (rows != null) {
                res.json(rows);
            }
        });
    });
});


/**
 *
 */
router.put('/update', function (req, res) {
    req.checkBody({
        'id': {
            notEmpty: true,
            errorMessage: "Invalid id"
        }
    });
    req.checkBody({
        'name': {
            notEmpty: true,
            errorMessage: "Invalid name"
        }
    });
    req.checkBody({
        'full_name': {
            notEmpty: true,
            errorMessage: "Invalid full_name"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var id = req.body.id;
        SocialActor.getById(id, function (err, rows) {
            if (err)
                res.json(err);

            if (!rows.length) {
                res.status(420);
                res.json({message: 'SocialActor already exists!'});
            } else {
                var object = req.body;
                object.modif_date = new Date().toMysqlFormat();
                SocialActor.update(object, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        res.json(rows);
                    }
                });
            }
        });
    });
});

/**
 * Deletes an object by its id
 * @param {req.body.id} the object's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.id) {
        SocialActor.delete(req.body.id, function (err, rows) {
            if (err)
                res.json(err);
            res.json(rows);
        });
    } else {
        res.send({error: 'No rows'});
    }
});


module.exports = router;
