// WorkTeamAssistant webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var WorkTeamAssistant = require('../../entity/team_members/work_team_assistant');
var Assistant = require('../../entity/team_members/assistant');
var WorkTeam = require('../../entity/team_members/work_team');

/**
 * Retrieves all the work team professors
 */
router.get('/findAll', function (req, res) {
    WorkTeamAssistant.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No assistant found'});
        }
    });
});

/**
 *  Retrieves all the objects matching the id received as param
 * @param {userId} input the user's id
 */
router.get('/findById/:id', function (req, res) {
    if (req.params.id) {
        WorkTeamAssistant.getById(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No assistant found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 * Retrieves all data by joining assistant table
 */
router.get('/findDataByTeam/:id', function (req, res) {
    if (req.params.id) {
        WorkTeamAssistant.getDataByTeamId(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No assistant found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});


/**
 * Creates a object from the provided params
 * @param {team_id} input the user's team_id
 * @param {assistant_id} input the user's professor_id
 */
router.post('/create', function (req, res) {
    req.checkBody({
        'team_id': {
            notEmpty: true,
            errorMessage: "Invalid team_id"
        }
    });
    req.checkBody({
        'assistant_id': {
            notEmpty: true,
            errorMessage: "Invalid assistant_id"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var team_id = req.body.team_id;
        WorkTeam.getById(team_id, function (err, teams) {
            if (err) {
                res.status(400);
                res.json(err);
            }
            if (teams.length) {
                var assistant_id = req.body.assistant_id;
                Assistant.getById(assistant_id, function (err, assistants) {
                    if (err) {
                        res.status(400);
                        res.json(err);
                    }
                    if (assistants.length) {
                        WorkTeamAssistant.create(req.body, function (err, rows) {
                            if (err) {
                                res.status(400);
                                res.send(err);
                            }
                            if (rows != null) {
                                res.status(200);
                                res.json(rows);
                            }
                        });
                    } else {
                        res.status(402);
                        res.json({message: 'Assistant not found!'});
                    }
                });
            } else {
                res.status(401);
                res.json({message: 'Team not found!'});
            }
        });
    });
});


/**
 * Deletes an object by its id
 * @param {team_id} the team's id
 * @param {assistant_id} the assistant's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.team_id && req.body.assistant_id) {
        WorkTeamAssistant.delete(req.body.team_id, req.body.assistant_id,
            function (err, rows) {
                if (err) {
                    res.status(420);
                    res.json(err);
                } else {
                    res.status(200);
                    res.json(rows);
                }

            });
    } else {
        res.status(300);
        res.send({error: 'No rows'});
    }
});


module.exports = router;
