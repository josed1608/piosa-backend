// Professor webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var Professor = require('../../entity/team_members/professor');

/**
 * Retrieves all the professors
 */
router.get('/findAll', function (req, res) {
    Professor.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No professors found'});
        }
    });
});

/**
 * Retrieves all the active professors
 */
router.get('/findActive', function (req, res) {
    Professor.getActive(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No professors found'});
        }
    });
});

/**
 *  Retrieves all the objects matching the id received as param
 * @param {userId} input the user's id
 */
router.get('/findById/:id', function (req, res) {
    if (req.params.id) {
        Professor.getById(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No entity found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 * Creates a object from the provided params
 * @param {name} input the user's name
 * @param {last_name} input the user's last_name
 * @param {sur_name} input the user's sur_name
 * @param {full_name} input the user's full_name
 */
router.post('/create', function (req, res) {
    req.checkBody({
        'name': {
            notEmpty: true,
            errorMessage: "Invalid name"
        }
    });
    req.checkBody({
        'full_name': {
            notEmpty: true,
            errorMessage: "Invalid full_name"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var name = req.body.name;
        Professor.getByName(name, function (err, rows) {
            if (err)
                res.json(err);

            if (rows.length) {
                res.status(420);
                res.json({message: 'Professor already exists!'});
            } else {
                var object = req.body;
                object.creation_date = new Date().toMysqlFormat();
                Professor.create(object, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        res.json(rows);
                    }
                });
            }
        });
    });
});

/**
 *
 */
router.put('/update', function (req, res) {
    req.checkBody({
        'id': {
            notEmpty: true,
            errorMessage: "Invalid id"
        }
    });
    req.checkBody({
        'name': {
            notEmpty: true,
            errorMessage: "Invalid name"
        }
    });
    req.checkBody({
        'full_name': {
            notEmpty: true,
            errorMessage: "Invalid full_name"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var id = req.body.id;
        Professor.getById(id, function (err, rows) {
            if (err)
                res.json(err);

            if (!rows.length) {
                res.status(420);
                res.json({message: 'Proyect already exists!'});
            } else {
                var object = req.body;
                object.modif_date = new Date().toMysqlFormat();
                Professor.update(object, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        res.json(rows);
                    }
                });
            }
        });
    });
});

/**
 * Deletes an object by its id
 * @param {req.body.id} the object's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.id) {
        Professor.delete(req.body.id, function (err, rows) {
            if (err)
                res.json(err);
            res.json(rows);
        });
    } else {
        res.send({error: 'No rows'});
    }
});


module.exports = router;
