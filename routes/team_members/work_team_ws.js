// WorkTeam webservice handler

var express = require('express');
var util = require('util');
var router = express.Router();
var WorkTeam = require('../../entity/team_members/work_team');
var WorkTeamAssistant = require('../../entity/team_members/work_team_assistant');
var WorkTeamProfessor = require('../../entity/team_members/work_team_professor');

/**
 * Retrieves all the work teams
 */
router.get('/findAll', function (req, res) {
    WorkTeam.getAll(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No professors found'});
        }
    });
});

/**
 * Retrieves all the active work teams
 */
router.get('/findActive', function (req, res) {
    WorkTeam.getActive(function (err, rows) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
        }
        if (rows.length) {
            res.status(200);
            res.statusMessage = "OK";
            res.json(rows);
        }
        else {
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No professors found'});
        }
    });
});

/**
 *  Retrieves all the objects matching the id received as param
 * @param {userId} input the user's id
 */
router.get('/findById/:id', function (req, res) {
    if (req.params.id) {
        WorkTeam.getById(req.params.id, function (err, rows) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }
            if (rows != null) {
                if (rows.length) {
                    res.status(200);
                    res.statusMessage = "OK";
                    res.json(rows);
                    return;
                }
            }
            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No entity found'});

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

router.get('/findAllData', function (req, res) {
    WorkTeam.getActive(function (err, workTeams) {
        if (err) {
            res.status(420);
            res.statusMessage = "Not found";
            res.json(err);
            return;
        }

        if (workTeams.length > 0) {
            workTeams.forEach(function (workTeam, i) {
                WorkTeamProfessor.getDataByTeamId(workTeam.id, function (err, professors) {
                    if (err) {
                        res.status(420);
                        res.statusMessage = "Not found";
                        res.json(err);
                        return;
                    }

                    workTeam.professors = professors;
                    WorkTeamAssistant.getDataByTeamId(workTeam.id, function (err, assistants) {
                        workTeam.assistants = assistants;

                        console.log(i);
                        if (i == workTeams.length-1) {
                            res.status(200);
                            res.statusMessage = "OK";
                            res.json(workTeams);
                        }

                    });
                });


            });

        } else {

            res.status(300);
            res.statusMessage = "No data";
            res.send({error: 'No entity found'});
        }

    });
});

/**
 * Data by Team
 */
router.get('/findDataByTeam/:id', function (req, res) {
    if (req.params.id) {
        WorkTeam.getById(req.params.id, function (err, workTeams) {
            if (err) {
                res.status(420);
                res.statusMessage = "Not found";
                res.json(err);
                return;
            }

            if (workTeams.length > 0) {
                var workTeam = workTeams[0];
                WorkTeamProfessor.getDataByTeamId(req.params.id, function (err, professors) {
                    if (err) {
                        res.status(420);
                        res.statusMessage = "Not found";
                        res.json(err);
                        return;
                    }

                    workTeam.professors = professors;
                    // res.status(200);
                    // res.statusMessage = "OK";
                    // res.json(workTeam);
                    WorkTeamAssistant.getDataByTeamId(req.params.id, function (err, assistants) {
                        workTeam.assistants = assistants;
                        res.status(200);
                        res.statusMessage = "OK";
                        res.json(workTeam);

                    });

                });

            } else {

                res.status(300);
                res.statusMessage = "No data";
                res.send({error: 'No entity found'});
            }

        });
    } else {
        res.json(err);
        res.status(420);
        res.statusMessage = "Not found";
        res.json(err);
    }
});

/**
 * Creates a object from the provided params
 * @param {name} input the user's name
 * @param {description} input the user's last_name
 * @param {coordinator_id} input the user's sur_name
 */
router.post('/create', function (req, res) {
    req.checkBody({
        'name': {
            notEmpty: true,
            errorMessage: "Invalid name"
        }
    });
    req.checkBody({
        'description': {
            notEmpty: true,
            errorMessage: "Invalid last_name"
        }
    });
    req.checkBody({
        'coordinator_id': {
            notEmpty: true,
            errorMessage: "Invalid coordinator_id"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var name = req.body.name;
        WorkTeam.getByName(name, function (err, rows) {
            if (err)
                res.json(err);

            if (rows.length) {
                res.status(420);
                res.json({message: 'WorkTeam already exists!'});

            } else {

                var object = req.body;
                object.modif_date = new Date().toMysqlFormat();
                object.creation_date = new Date().toMysqlFormat();
                WorkTeam.create(object, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        res.status(200);
                        res.json(rows);
                    }
                });
            }
        });
    });
});

/**
 *
 */
router.put('/update', function (req, res) {
    req.checkBody({
        'id': {
            notEmpty: true,
            errorMessage: "Invalid id"
        }
    });
    req.checkBody({
        'name': {
            notEmpty: true,
            errorMessage: "Invalid name"
        }
    });
    req.checkBody({
        'description': {
            notEmpty: true,
            errorMessage: "Invalid last_name"
        }
    });
    req.checkBody({
        'coordinator_id': {
            notEmpty: true,
            errorMessage: "Invalid coordinator_id"
        }
    });
    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(420);
            res.send({error: 'There have been validation errors: ' + util.inspect(result.array())});
            return;
        }
        var id = req.body.id;
        WorkTeam.getById(id, function (err, rows) {
            if (err)
                res.json(err);

            if (rows.length) {

                var object = req.body;
                object.modif_date = new Date().toMysqlFormat();
                // object.creation_date = new Date().toMysqlFormat();
                WorkTeam.update(object, function (err, rows) {
                    if (err)
                        res.send(err);
                    if (rows != null) {
                        res.status(200);
                        res.json(rows);
                    }
                });

            } else {
                res.status(420);
                res.json({message: 'WorkTeam already exists!'});

            }
        });
    });
});


/**
 * Deletes an object by its id
 * @param {req.body.id} the object's id
 */
router.delete('/delete', function (req, res) {
    if (req.body.id) {
        WorkTeam.delete(req.body.id, function (err, rows) {
            if (err)
                res.json(err);
            res.json(rows);
        });
    } else {
        res.send({error: 'No rows'});
    }
});


module.exports = router;
