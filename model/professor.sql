
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (1,'Lochi','Yu');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (2,'Maripaz','Castro');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (3,'Milena','Castro');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (4,'Pedro','Vargas');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (5,'Teodoro','Willink','Castro');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (6,'Yorleny','Araya');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (7,'Lea','Wexler');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (8,'Elba','Cubero');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (9,'Juan José','Alvarado','Barrientos');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (10,'Eduardo','Thompson');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (11,'Gerardo','Cortés','Muñoz');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (12,'Daisy','Arrollo','Mora');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (13,'Cindy','Fernández','García');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (14,'Albert','Campos','Arguello');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (15,'Julieta','Carranza','Velázquez');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (16,'Karla Vanessa ','Rojas','Herrera');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (17,'José Francisco','Diestéfano','Galdofi');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (18,'Walter','Marín');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (19,'Ana Lucía','Mayorga','Cross');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (20,'Adrián','Pinto');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (21,'Guaria','Cárdenas','Sandí');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (22,'Yolanda','Camacho');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (23,'Rebeca','Mora');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (24,'Catalina','Murillo');

--
---- WORK TEAM
--INSERT INTO k_piosa.work_team(id, name) VALUES (1,'Nucleo Coordinador');
--
---- WORK TEAM PROFESSOR
--INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 14);
--INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 13);
--INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 9);
--INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 1);
--INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 3);
--INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 5);

