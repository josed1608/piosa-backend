CREATE DATABASE IF NOT EXISTS k_piosa
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_general_ci;

-- ADMIN USER
CREATE TABLE IF NOT EXISTS k_piosa.admin_user(
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(200),
  active BOOLEAN,
  confirmed BOOLEAN,
  email VARCHAR(100)NOT NULL UNIQUE,
  password VARCHAR(200),
  creation_date DATETIME,
  modif_date DATETIME,
  PRIMARY KEY(id)
);

-- VERIFICATION TOKEN
CREATE TABLE IF NOT EXISTS k_piosa.admin_user_token(
  admin_id INT NOT NULL,
  token VARCHAR(200),
  expiration_date DATETIME,
  used BOOLEAN,

FOREIGN KEY(admin_id)
REFERENCES k_piosa.admin_user(id)
  ON DELETE CASCADE,

  PRIMARY KEY(admin_id, token)
);


-- ENTITY
CREATE TABLE IF NOT EXISTS k_piosa.entity(
  id INT NOT NULL,
  name VARCHAR(32) NOT NULL UNIQUE,
  PRIMARY KEY(id)
);

-- ROLE TYPE
-- READ - WRITE - ADMIN
CREATE TABLE IF NOT EXISTS k_piosa.role_type(
  id INT NOT NULL,
  name VARCHAR(100) NOT NULL UNIQUE,
PRIMARY KEY(id)
);

-- El tipo de usuario del sistema.
-- Coordinator -Professor -Assistant -Visitor - Admin
CREATE TABLE IF NOT EXISTS k_piosa.admin_type(
  id INT NOT NULL,
  name VARCHAR(100) NOT NULL UNIQUE,
PRIMARY KEY(id)
);

-- Set de roles asociados al admin
CREATE TABLE IF NOT EXISTS k_piosa.admin_type_role(
type_id INT NOT NULL,
entity_id INT NOT NULL,
role_type INT NOT NULL,

FOREIGN KEY(entity_id)
REFERENCES k_piosa.entity(id)
  ON DELETE CASCADE,

FOREIGN KEY(type_id)
REFERENCES k_piosa.admin_type(id)
  ON DELETE CASCADE,

FOREIGN KEY(role_type)
REFERENCES k_piosa.role_type(id)
  ON DELETE CASCADE,

PRIMARY KEY(type_id, entity_id, role_type)
);

-- ADMIN ROLE
CREATE TABLE IF NOT EXISTS k_piosa.admin_role(
  id INT NOT NULL AUTO_INCREMENT,
  admin_id INT NOT NULL,
  entity_id INT NOT NULL,
  role_type INT NOT NULL,

  FOREIGN KEY(admin_id)
  REFERENCES k_piosa.admin_user(id)
  ON DELETE CASCADE,

FOREIGN KEY(entity_id)
REFERENCES k_piosa.entity(id)
  ON DELETE CASCADE,

PRIMARY KEY(id, admin_id, entity_id)
);

-- LOGS FOR THE SYSTEM
CREATE TABLE IF NOT EXISTS k_piosa.admin_log(
  creation_date DATETIME,
  created_by INT NOT NULL UNIQUE,
  entity_id INT NOT NULL,
  action VARCHAR(20),
  detail VARCHAR(200),

FOREIGN KEY(created_by)
REFERENCES k_piosa.admin_user(id)
  ON DELETE NO ACTION,

FOREIGN KEY(entity_id)
REFERENCES k_piosa.entity(id)
  ON DELETE NO ACTION,

PRIMARY KEY(creation_date, created_by, entity_id)
);

-- USER
CREATE TABLE IF NOT EXISTS k_piosa.user(
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100),
  last_name VARCHAR(100),
  sur_name VARCHAR(100),
  profile_img VARCHAR(200),
  active BOOLEAN,
  confirmed BOOLEAN,
  email VARCHAR(100)NOT NULL UNIQUE,
  code VARCHAR(32),
  password VARCHAR(200),
  creation_date DATETIME,
  modif_date DATETIME,
  PRIMARY KEY(id)
);

-- TAG
CREATE TABLE IF NOT EXISTS k_piosa.tag(
  id INT NOT NULL AUTO_INCREMENT,
  tag VARCHAR(100) UNIQUE,
  PRIMARY KEY(id)
);


-- REGION
CREATE TABLE IF NOT EXISTS k_piosa.region(
  id INT NOT NULL,
  name VARCHAR(32) UNIQUE,
    PRIMARY KEY(id)
);


-- PROYECT TYPE
CREATE TABLE IF NOT EXISTS k_piosa.project_type(
  id INT NOT NULL,
  name VARCHAR(32) UNIQUE,
PRIMARY KEY(id)
);


-- PROYECT
CREATE TABLE IF NOT EXISTS k_piosa.project(
  id VARCHAR(32) NOT NULL,
  type INT NOT NULL,
  name VARCHAR(300),
  description VARCHAR(200),
  img_url VARCHAR(200),
  start_date DATETIME,
  end_date DATETIME,
  creation_date DATETIME,
  modif_date DATETIME,
  active BOOLEAN,
  FOREIGN KEY(type)
  REFERENCES k_piosa.project_type(id)
  ON DELETE NO ACTION,

PRIMARY KEY(id)
);



-- ADVANCE
CREATE TABLE IF NOT EXISTS k_piosa.advance(
  id INT NOT NULL AUTO_INCREMENT,
  title VARCHAR(100),
  description VARCHAR(200),
  project_id VARCHAR(32) NOT NULL,
  creation_date DATETIME,
  modif_date DATETIME,
  active BOOLEAN,
  FOREIGN KEY(project_id)
  REFERENCES k_piosa.project(id)
  ON DELETE CASCADE,
  PRIMARY KEY(id)
);

-- EVENTO
CREATE TABLE IF NOT EXISTS k_piosa.event(
  id INT NOT NULL AUTO_INCREMENT,
  title VARCHAR(100),
  description VARCHAR(200),
  start DATETIME,
  end DATETIME,
  start_hour TIME,
  end_hour TIME,
  img_url VARCHAR(200),
  project_id VARCHAR(32) NOT NULL,
  creation_date DATETIME,
  modif_date DATETIME,
  active BOOLEAN,
  FOREIGN KEY(project_id)
  REFERENCES k_piosa.project(id)
  ON DELETE CASCADE,
  PRIMARY KEY(id)
);


-- ARTICLE

CREATE TABLE IF NOT EXISTS k_piosa.article(
  id INT NOT NULL AUTO_INCREMENT,
  title VARCHAR(100),
  resume VARCHAR(300),
  content VARCHAR(320),
  facebook_id VARCHAR(100),
  link VARCHAR(200),
  img_url VARCHAR(200),
  project_id VARCHAR(32) NOT NULL,
  creation_date DATETIME,
  modif_date DATETIME,
  active BOOLEAN,
  FOREIGN KEY(project_id)
  REFERENCES k_piosa.project(id)
  ON DELETE CASCADE,
  PRIMARY KEY(id)
);

-- FACEBOOK FEED

CREATE TABLE IF NOT EXISTS k_piosa.facebook_feed(
  id VARCHAR(100) NOT NULL,
  message VARCHAR(1000),
  link VARCHAR(300),
  full_picture VARCHAR(320),
  created_time DATETIME,
PRIMARY KEY(id)
);

-- SOCIAL ACTOR
CREATE TABLE IF NOT EXISTS k_piosa.social_actor(
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(100) UNIQUE,
  detail VARCHAR(200),
  location VARCHAR(200),
  description VARCHAR(320),
  name VARCHAR(32),
  last_name VARCHAR(32),
  sur_name VARCHAR(32),
  img_url VARCHAR(200),
  active BOOLEAN,
  creation_date DATETIME,
  modif_date DATETIME,
PRIMARY KEY(id)
);

-- PROFESSOR
CREATE TABLE IF NOT EXISTS k_piosa.professor(
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(100) UNIQUE,
  detail VARCHAR(200),
  description VARCHAR(320),
  name VARCHAR(32),
  last_name VARCHAR(32),
  sur_name VARCHAR(32),
  img_url VARCHAR(200),
  active BOOLEAN,
  creation_date DATETIME,
  modif_date DATETIME,
  PRIMARY KEY(id)
);

-- ASSISTANT
CREATE TABLE IF NOT EXISTS k_piosa.assistant(
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(100) UNIQUE,
  name VARCHAR(32),
  detail VARCHAR(200),
  description VARCHAR(320),
  last_name VARCHAR(32),
  sur_name VARCHAR(32),
  img_url VARCHAR(200),
  creation_date DATETIME,
  modif_date DATETIME,
  active BOOLEAN,
  PRIMARY KEY(id)
);


-- REGION
CREATE TABLE IF NOT EXISTS k_piosa.academic_unit(
  id INT NOT NULL,
  name VARCHAR(256) UNIQUE,
    PRIMARY KEY(id)
);


-- PROYECT COORDINATOR
CREATE TABLE IF NOT EXISTS k_piosa.project_coordinator(
  project_id VARCHAR(32) NOT NULL ,
  coordinator_id INT NOT NULL,

  FOREIGN KEY(project_id)
  REFERENCES k_piosa.project(id)
  ON DELETE CASCADE,

  FOREIGN KEY(coordinator_id)
  REFERENCES k_piosa.professor(id)
  ON DELETE CASCADE,

  PRIMARY KEY(project_id, coordinator_id)
);

-- PROYECT COLABORATOR
CREATE TABLE IF NOT EXISTS k_piosa.project_collaborator(
  project_id VARCHAR(32) NOT NULL ,
  collaborator_id INT NOT NULL,

  FOREIGN KEY(project_id)
  REFERENCES k_piosa.project(id)
  ON DELETE CASCADE,

  FOREIGN KEY(collaborator_id)
  REFERENCES k_piosa.social_actor(id)
  ON DELETE CASCADE,

  PRIMARY KEY(project_id, collaborator_id)
);


-- PROYECT TAG
CREATE TABLE IF NOT EXISTS k_piosa.project_tag(
 project_id VARCHAR(32) NOT NULL ,
  tag_id INT NOT NULL,

FOREIGN KEY(project_id)
REFERENCES k_piosa.project(id)
ON DELETE CASCADE,

FOREIGN KEY(tag_id)
REFERENCES k_piosa.tag(id)
  ON DELETE CASCADE,

PRIMARY KEY(project_id, tag_id)
);


-- PROFESSOR ACADEMIC UNIT
CREATE TABLE IF NOT EXISTS k_piosa.professor_academic_unit(
  professor_id INT NOT NULL,
    unit_id INT NOT NULL,

FOREIGN KEY(professor_id)
REFERENCES k_piosa.professor(id)
ON DELETE CASCADE,

FOREIGN KEY(unit_id)
REFERENCES k_piosa.academic_unit(id)
  ON DELETE CASCADE,

    PRIMARY KEY(professor_id, unit_id)
);



-- PROYECT LOCATION
CREATE TABLE IF NOT EXISTS k_piosa.project_location(
  id INT NOT NULL AUTO_INCREMENT,
  project_id VARCHAR(32) NOT NULL ,
  latitude DECIMAL(10, 8),
  longitude DECIMAL(10, 8),
  tag VARCHAR (32),

FOREIGN KEY(project_id)
REFERENCES k_piosa.project(id)
ON DELETE CASCADE,

    PRIMARY KEY(id , project_id)
);

-- WORK TEAM
CREATE TABLE IF NOT EXISTS k_piosa.work_team(
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) UNIQUE,
  description VARCHAR(320),
  active BOOLEAN,
  coordinator_id INT NOT NULL,

  creation_date DATETIME,
  modif_date DATETIME,

  FOREIGN KEY(coordinator_id)
  REFERENCES k_piosa.professor(id)
  ON DELETE NO ACTION,

  PRIMARY KEY(id)
);

-- WORK TEAM PROFESSOR

CREATE TABLE IF NOT EXISTS k_piosa.work_team_professor(
  team_id INT NOT NULL,
  professor_id INT NOT NULL,
  FOREIGN KEY(team_id)
  REFERENCES k_piosa.work_team(id)
  ON DELETE CASCADE,
  FOREIGN KEY(professor_id)
  REFERENCES k_piosa.professor(id)
  ON DELETE CASCADE,
  PRIMARY KEY(team_id, professor_id)
);


-- WORK TEAM ASSISTANT

CREATE TABLE IF NOT EXISTS k_piosa.work_team_assistant(
  team_id INT NOT NULL,
  assistant_id INT NOT NULL,
  FOREIGN KEY(team_id)
  REFERENCES k_piosa.work_team(id)
  ON DELETE CASCADE,
  FOREIGN KEY(assistant_id)
  REFERENCES k_piosa.assistant(id)
  ON DELETE CASCADE,
  PRIMARY KEY(team_id, assistant_id )
);


-- ARTICLE TAG
CREATE TABLE IF NOT EXISTS k_piosa.article_tag(
 article_id INT NOT NULL ,
 tag_id INT NOT NULL,

FOREIGN KEY(article_id)
REFERENCES k_piosa.article(id)
ON DELETE CASCADE,

FOREIGN KEY(tag_id)
REFERENCES k_piosa.tag(id)
  ON DELETE CASCADE,

PRIMARY KEY(article_id, tag_id)
);


-- ADVANCE TAG
CREATE TABLE IF NOT EXISTS k_piosa.advance_tag(
  advance_id INT NOT NULL ,
  tag_id INT NOT NULL,

FOREIGN KEY(advance_id)
REFERENCES k_piosa.advance(id)
ON DELETE CASCADE,

FOREIGN KEY(tag_id)
REFERENCES k_piosa.tag(id)
  ON DELETE CASCADE,

PRIMARY KEY(advance_id, tag_id)
);


-- EVENT TAG
CREATE TABLE IF NOT EXISTS k_piosa.event_tag(
 event_id INT NOT NULL ,
 tag_id INT NOT NULL,

FOREIGN KEY(event_id)
REFERENCES k_piosa.event(id)
ON DELETE CASCADE,

FOREIGN KEY(tag_id)
REFERENCES k_piosa.tag(id)
  ON DELETE CASCADE,

PRIMARY KEY(event_id, tag_id)
);
