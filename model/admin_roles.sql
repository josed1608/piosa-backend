
-- ADMIN USER TYPE
INSERT INTO k_piosa.admin_type(id, name) VALUES (1, 'Administrador');
INSERT INTO k_piosa.admin_type(id, name) VALUES (2, 'Coordinador');
INSERT INTO k_piosa.admin_type(id, name) VALUES (3, 'Profesor');
INSERT INTO k_piosa.admin_type(id, name) VALUES (4, 'Assistente');

-- ROLE  TYPE
INSERT INTO k_piosa.role_type(id, name) VALUES (1, 'Administrador');
INSERT INTO k_piosa.role_type(id, name) VALUES (2, 'Lectura');
INSERT INTO k_piosa.role_type(id, name) VALUES (3, 'Escritura');

-- ADMIN TYPE ROLE

-- Administrador
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 1, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 2, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 3, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 4, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 5, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 6, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 7, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 8, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 9, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 10, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 11, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 12, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 13, 1);