
-- ENTITIY DATA
INSERT INTO k_piosa.entity(id, name) VALUES (1, 'Usuario Administrador');
INSERT INTO k_piosa.entity(id, name) VALUES (2, 'Roles de Administrador');
INSERT INTO k_piosa.entity(id, name) VALUES (3, 'Usuario');
INSERT INTO k_piosa.entity(id, name) VALUES (4, 'Anuncio');
INSERT INTO k_piosa.entity(id, name) VALUES (5, 'Avance');
INSERT INTO k_piosa.entity(id, name) VALUES (6, 'Evento');
INSERT INTO k_piosa.entity(id, name) VALUES (7, 'Noticia');
INSERT INTO k_piosa.entity(id, name) VALUES (8, 'Profesor');
INSERT INTO k_piosa.entity(id, name) VALUES (9, 'Proyecto');
INSERT INTO k_piosa.entity(id, name) VALUES (10, 'Equipo de Trabajo');
INSERT INTO k_piosa.entity(id, name) VALUES (11, 'Asistente');
INSERT INTO k_piosa.entity(id, name) VALUES (12, 'Discurso');
INSERT INTO k_piosa.entity(id, name) VALUES (13, 'Etiqueta');

-- ADMIN USER TYPE
INSERT INTO k_piosa.admin_type(id, name) VALUES (1, 'Administrador');
INSERT INTO k_piosa.admin_type(id, name) VALUES (2, 'Coordinador');
INSERT INTO k_piosa.admin_type(id, name) VALUES (3, 'Profesor');
INSERT INTO k_piosa.admin_type(id, name) VALUES (4, 'Assistente');

-- ROLE  TYPE
INSERT INTO k_piosa.role_type(id, name) VALUES (1, 'Administrador');
INSERT INTO k_piosa.role_type(id, name) VALUES (2, 'Lectura');
INSERT INTO k_piosa.role_type(id, name) VALUES (3, 'Escritura');

-- ADMIN TYPE ROLE

-- Administrador
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 1, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 2, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 3, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 4, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 5, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 6, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 7, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 8, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 9, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 10, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 11, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 12, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (1, 13, 1);

-- Coordinador
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (2, 1, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (2, 2, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (2, 3, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (2, 4, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (2, 5, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (2, 6, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (2, 7, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (2, 8, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (2, 9, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (2, 10, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (2, 11, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (2, 12, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (2, 13, 1);

-- Profesor
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (3, 1, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (3, 2, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (3, 3, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (3, 4, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (3, 5, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (3, 6, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (3, 7, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (3, 8, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (3, 9, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (3, 10, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (3, 11, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (3, 12, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (3, 13, 1);

-- Asistente
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (4, 1, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (4, 2, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (4, 3, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (4, 4, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (4, 5, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (4, 6, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (4, 7, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (4, 8, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (4, 9, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (4, 10, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (4, 11, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (4, 12, 1);
INSERT INTO k_piosa.admin_type_role(type_id, entity_id, role_type) VALUES (4, 13, 1);

-- REGION
INSERT INTO k_piosa.region(id, name) VALUES (1, 'Osa');

-- PROYECT TYPE
INSERT INTO k_piosa.project_type(id, name) VALUES (1, 'Acción Social');
INSERT INTO k_piosa.project_type(id, name) VALUES (2, 'Investigación');
INSERT INTO k_piosa.project_type(id, name) VALUES (3, 'Docencia');

-- PROYECT

-- ACCION SOCIAL
INSERT INTO k_piosa.project(id, name, type) VALUES ('TC-999', 'Salud animal', 1);
INSERT INTO k_piosa.project(id, name, type) VALUES ('TC-691', 'Tropicalización de la tecnología', 1);
INSERT INTO k_piosa.project(id, name, type) VALUES ('TC-675', 'Taller de investigación de salud comunitaria', 1);
INSERT INTO k_piosa.project(id, name, type) VALUES ('TC-581', 'Capacitación a pobladores de las zonas costeras en manejo y protección de los recursos marinos', 1);
INSERT INTO k_piosa.project(id, name, type) VALUES ('TC-230', 'Desarrollo Agroindustrial', 1);
INSERT INTO k_piosa.project(id, name, type) VALUES ('TC-228', 'Asesoría agroeconómica a organizaciones de productores agropecuarios y agroindustriales', 1);
INSERT INTO k_piosa.project(id, name, type) VALUES ('EC-330', 'ElectrizArte', 1);

-- INVESTIGACION
INSERT INTO k_piosa.project(id, name, type) VALUES ('822-B2-A05', 'Estudios en economía agrícola, salud y ambiente', 2);
INSERT INTO k_piosa.project(id, name, type) VALUES ('B5-735-050', 'Valoración de matrices alimentarias mediante la evaluación del impacto del procesamiento sobre el perfil metabolómico', 2);
INSERT INTO k_piosa.project(id, name, type) VALUES ('808-B6-520', 'Evaluación del impacto del Niño-Godzila en la condición de los ecosistemas arrecifales de las áreas de conservación Osa y Marina, Isla del Coco, Pacífico, Costa Rica', 2);
INSERT INTO k_piosa.project(id, name, type) VALUES ('808-B6-171', 'Diversidad, distribución y abundancia de moluscos de manglar del estero Monolobo, Coto Colorado, Golfo Dulce, Costa Rica', 2);
INSERT INTO k_piosa.project(id, name, type) VALUES ('808-B4-717', 'Catálogo digital de macroalgas marinas del Pacífico de Costa Rica', 2);
INSERT INTO k_piosa.project(id, name, type) VALUES ('801-B2-608', 'Manejo alternativo de sompopaz (géneros Atta y Acromyrmex) mediante el uso de microorganismos entomopatógenos y antagonistas del hongo cultivado por las hormigas: enfoque multidisciplinario para solucionar un problema del agro costarricense', 2);
INSERT INTO k_piosa.project(id, name, type) VALUES ('731-B3-020', 'Degradación de la madera por macrohongos, su relación con el flujo de nutrientes y la regeneración natural en un bosque tropical del Pacífico Sur y Central de Costa Rica', 2);
INSERT INTO k_piosa.project(id, name, type) VALUES ('113-B6-134', 'Evaluación del índice de erosión-sedimentación costera en el sector noroeste de la península de Burica, Pacífico Sur de Costa Rica', 2);


-- PROFESORES
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (1,'Lochi','Yu');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (2,'Maripaz','Castro');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (3,'Milena','Castro');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (4,'Pedro','Vargas');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (5,'Teodoro','Willink','Castro');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (6,'Yorleny','Araya');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (7,'Lea','Wexler');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (8,'Elba','Cubero');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (9,'Juan José','Alvarado','Barrientos');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (10,'Eduardo','Thompson');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (11,'Gerardo','Cortés','Muñoz');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (12,'Daisy','Arrollo','Mora');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (13,'Cindy','Fernández','García');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (14,'Albert','Campos','Arguello');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (15,'Julieta','Carranza','Velázquez');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (16,'Karla Vanessa ','Rojas','Herrera');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (17,'José Francisco','Diestéfano','Galdofi');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (18,'Walter','Marín');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (19,'Ana Lucía','Mayorga','Cross');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (20,'Adrián','Pinto');
INSERT INTO k_piosa.professor(id, name, last_name, sur_name) VALUES (21,'Guaria','Cárdenas','Sandí');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (22,'Yolanda','Camacho');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (23,'Rebeca','Mora');
INSERT INTO k_piosa.professor(id, name, last_name) VALUES (24,'Catalina','Murillo');


-- ASSISTANT
INSERT INTO k_piosa.assistant(id, name, last_name, sur_name) VALUES (1,'Nathaly','Montero','Solís');
INSERT INTO k_piosa.assistant(id, name, last_name, sur_name) VALUES (2,'Lena','Pizarro','Peraza');


-- WORK TEAM
INSERT INTO k_piosa.work_team(id, name, coordinator_id) VALUES (1,'Nucleo Coordinador', 1);

-- WORK TEAM PROFESSOR
INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 14);
INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 13);
INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 9);
INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 1);
INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 3);
INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 5);


-- WORK TEAM ASSISTANT
INSERT INTO k_piosa.work_team_assistant(team_id, assistant_id) VALUES (1, 1);
INSERT INTO k_piosa.work_team_assistant(team_id, assistant_id) VALUES (1, 2);


select work_team.name as team_name,
       professor.name as professor_name, assistant.name as assistant_name
from work_team join work_team_professor on work_team.id = work_team_professor.team_id
  join work_team_assistant on work_team.id = work_team_assistant.team_id
  join professor on work_team_professor.professor_id = professor.id
  join assistant on work_team_assistant.assistant_id = assistant.id where work_team.id = 1 group by work_team.name;


select professor.name as professor_name from work_team
  join work_team_professor on work_team.id = work_team_professor.team_id
  join professor on work_team_professor.professor_id = professor.id where work_team.id = 1;

select assistant.name as assistant_name from work_team
  join work_team_assistant on work_team.id = work_team_assistant.team_id
  join assistant on work_team_assistant.assistant_id = assistant.id where work_team.id = 1;


scp -i "/Users/konradjimenezc/kdog-key-pair.pem" ec2-user@ec2-13-58-32-238.us-east-2.compute.amazonaws.com:konrad.sql $HOME