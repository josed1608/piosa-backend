
-- ENTITIY DATA
INSERT INTO k_piosa.entity(id, name) VALUES (1, 'Usuario Administrador');
INSERT INTO k_piosa.entity(id, name) VALUES (2, 'Roles de Administrador');
INSERT INTO k_piosa.entity(id, name) VALUES (3, 'Usuario');
INSERT INTO k_piosa.entity(id, name) VALUES (4, 'Anuncio');
INSERT INTO k_piosa.entity(id, name) VALUES (5, 'Avance');
INSERT INTO k_piosa.entity(id, name) VALUES (6, 'Evento');
INSERT INTO k_piosa.entity(id, name) VALUES (7, 'Noticia');
INSERT INTO k_piosa.entity(id, name) VALUES (8, 'Profesor');
INSERT INTO k_piosa.entity(id, name) VALUES (9, 'Proyecto');
INSERT INTO k_piosa.entity(id, name) VALUES (10, 'Equipo de Trabajo');
INSERT INTO k_piosa.entity(id, name) VALUES (11, 'Asistente');
INSERT INTO k_piosa.entity(id, name) VALUES (12, 'Discurso');
INSERT INTO k_piosa.entity(id, name) VALUES (13, 'Etiqueta');
