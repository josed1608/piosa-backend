
-- PROYECT TYPE
INSERT INTO k_piosa.project_type(id, name) VALUES (1, 'Acción Social');
INSERT INTO k_piosa.project_type(id, name) VALUES (2, 'Investigación');
INSERT INTO k_piosa.project_type(id, name) VALUES (3, 'Docencia');

-- PROYECT

-- ACCION SOCIAL
INSERT INTO k_piosa.project(id, name, type) VALUES ('TC-999', 'Salud animal', 1);
INSERT INTO k_piosa.project(id, name, type) VALUES ('TC-691', 'Tropicalización de la tecnología', 1);
INSERT INTO k_piosa.project(id, name, type) VALUES ('TC-675', 'Taller de investigación de salud comunitaria', 1);
INSERT INTO k_piosa.project(id, name, type) VALUES ('TC-581', 'Capacitación a pobladores de las zonas costeras en manejo y protección de los recursos marinos', 1);
INSERT INTO k_piosa.project(id, name, type) VALUES ('TC-230', 'Desarrollo Agroindustrial', 1);
INSERT INTO k_piosa.project(id, name, type) VALUES ('TC-228', 'Asesoría agroeconómica a organizaciones de productores agropecuarios y agroindustriales', 1);
INSERT INTO k_piosa.project(id, name, type) VALUES ('EC-330', 'ElectrizArte', 1);

-- INVESTIGACION
INSERT INTO k_piosa.project(id, name, type) VALUES ('822-B2-A05', 'Estudios en economía agrícola, salud y ambiente', 2);
INSERT INTO k_piosa.project(id, name, type) VALUES ('B5-735-050', 'Valoración de matrices alimentarias mediante la evaluación del impacto del procesamiento sobre el perfil metabolómico', 2);
INSERT INTO k_piosa.project(id, name, type) VALUES ('808-B6-520', 'Evaluación del impacto del Niño-Godzila en la condición de los ecosistemas arrecifales de las áreas de conservación Osa y Marina, Isla del Coco, Pacífico, Costa Rica', 2);
INSERT INTO k_piosa.project(id, name, type) VALUES ('808-B6-171', 'Diversidad, distribución y abundancia de moluscos de manglar del estero Monolobo, Coto Colorado, Golfo Dulce, Costa Rica', 2);
INSERT INTO k_piosa.project(id, name, type) VALUES ('808-B4-717', 'Catálogo digital de macroalgas marinas del Pacífico de Costa Rica', 2);
INSERT INTO k_piosa.project(id, name, type) VALUES ('801-B2-608', 'Manejo alternativo de sompopaz (géneros Atta y Acromyrmex) mediante el uso de microorganismos entomopatógenos y antagonistas del hongo cultivado por las hormigas: enfoque multidisciplinario para solucionar un problema del agro costarricense', 2);
INSERT INTO k_piosa.project(id, name, type) VALUES ('731-B3-020', 'Degradación de la madera por macrohongos, su relación con el flujo de nutrientes y la regeneración natural en un bosque tropical del Pacífico Sur y Central de Costa Rica', 2);
INSERT INTO k_piosa.project(id, name, type) VALUES ('113-B6-134', 'Evaluación del índice de erosión-sedimentación costera en el sector noroeste de la península de Burica, Pacífico Sur de Costa Rica', 2);
