
-- WORK TEAM
INSERT INTO k_piosa.work_team(id, name) VALUES (1,'Nucleo Coordinador');

-- WORK TEAM PROFESSOR
INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 14);
INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 13);
INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 9);
INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 1);
INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 3);
INSERT INTO k_piosa.work_team_professor(team_id, professor_id) VALUES (1, 5);

