var db = require('../../dbconnection');

var User = {

    getAll: function (callback) {
        return db.query("select * from user", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from user where id = ?", [id], callback);
    },

    getByEmail: function (email, callback) {
        console.log(email);
        return db.query("select * from user where email = ?", [email], callback);
    },

    create: function (object, callback) {
        return db.query("insert into user (id, name, email, password, creation_date) " +
            "values(?, ?, ?, ?, ?)",  [object.id, object.name, object.email, object.password, object.creation_date], function () {
                return db.query("select * from user where id = ?", [object.id], callback);
            });
    },

    update: function (object, callback) {
        return db.query("update user set name = ? , email = ? where id = ?", [object.name, object.email, object.id], callback);
    },

    delete: function (id, callback) {
        return db.query("delete from user where id = ? ", [id], callback);
    }

};

module.exports = User;