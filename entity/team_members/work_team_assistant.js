var db = require('../../dbconnection');

var WorkTeamAssistant = {

    getAll: function (callback) {
        return db.query("select * from work_team_assistant order by assistant_id asc", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from work_team_assistant where id = ?", [id], callback);
    },

    getDataByTeamId: function (id, callback) {
        return db.query("select work_team_assistant.team_id, work_team_assistant.assistant_id," +
            "assistant.name, assistant.last_name, assistant.sur_name, " +
            "assistant.img_url , assistant.description "+
            "from work_team_assistant " +
            "join assistant on assistant.id = work_team_assistant.assistant_id " +
            "where team_id = ?", [id], callback);
    },

    create: function (object, callback) {
        return db.query("insert into work_team_assistant (team_id, assistant_id) " +
            "values(?, ?)", [object.team_id, object.assistant_id], function () {
            return db.query("select * from work_team_assistant where team_id = ? and assistant_id = ?",
                [object.team_id, object.assistant_id], callback);
        });
    },

    delete: function (team, assistant, callback) {
        return db.query("delete from work_team_assistant where team_id = ? and assistant_id = ?",
            [team, assistant], callback);
    }

};

module.exports = WorkTeamAssistant;