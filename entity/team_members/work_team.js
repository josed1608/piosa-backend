var db = require('../../dbconnection');

var WorktTeam = {

    getAll: function (callback) {
        return db.query("select * from work_team order by id asc", callback);
    },

    getActive: function (callback) {
        return db.query("select * from work_team where active = true order by id asc", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from work_team where id = ? limit 1", id, callback);
    },

    getByName: function (name, callback) {
        return db.query("select * from work_team where name = ?", name, callback);
    },

    create: function (object, callback) {
        return db.query("insert into work_team (name, description, coordinator_id, active, creation_date, modif_date) " +
            "values(?, ?, ?, ?, ?, ?)", [object.name, object.description, object.coordinator_id, object.active, object.creation_date, object.modif_date],
            function () {
            return db.query("select * from work_team where name = ?", [object.name],
                callback);
        });
    },

    update: function (object, callback) {
        return db.query("update work_team set name = ?, description = ?, coordinator_id = ?, active = ?,  modif_date = ? where id = ?",
            [object.name, object.description, object.coordinator_id, object.active, object.modif_date, object.id],
            // function () {
            //     return db.query("select * from work_team where id = ?", [object.id],
                    callback);
            // });
    },

    delete: function (id, callback) {
        return db.query("delete from work_team where id = ? ", [id], callback);
    }

};

module.exports = WorktTeam;