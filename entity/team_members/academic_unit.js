var db = require('../../dbconnection');

var AcademicUnit = {

    getAll: function (callback) {
        return db.query("select * from academic_unit order by id asc", callback);
    },


    getById: function (id, callback) {
        return db.query("select * from academic_unit where id = ?", [id], callback);
    },

    getByName: function (name, callback) {
        return db.query("select * from academic_unit where name = ?", name, callback);
    },

    create: function (object, callback) {
        return db.query("insert into academic_unit (name) values(?)", [object.name], function () {
            return db.query("select * from academic_unit where name = ?", [object.name], callback);
        });
    },

    update: function (object, callback) {
        return db.query("update academic_unit set name = ? where id = ?", [object.name, object.id], function () {
            return db.query("select * from academic_unit where id = ?", [object.id], callback);
        });
    },

    delete: function (id, callback) {
        return db.query("delete from academic_unit where id = ? ", [id], callback);
    }

};

module.exports = AcademicUnit;