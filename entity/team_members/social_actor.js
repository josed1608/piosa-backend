var db = require('../../dbconnection');

var SocialActor = {

    getAll: function (callback) {
        return db.query("select * from social_actor order by id asc", callback);
    },

    getActive: function (callback) {
        return db.query("select * from social_actor where active = true order by id asc", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from social_actor where id = ?", [id], callback);
    },

    getByName: function (name, callback) {
        return db.query("select * from social_actor where name = ?", name, callback);
    },

    create: function (object, callback) {
        return db.query("insert into social_actor (name, last_name, sur_name, active, img_url, location, creation_date) " +
            "values(?, ?, ?, ?, ?, ?, ?)", [object.name, object.last_name, object.sur_name, bject.active, object.img_url, object.location, object.creation_date], function () {
            return db.query("select * from social_actor where name = ?", [object.name], callback);
        });
    },

    update: function (object, callback) {
        return db.query("update social_actor set name = ?, last_name = ?, sur_name = ?, active = ?, " +
            "img_url = ?, location = ?, modif_date = ? where id = ?",
            [object.name, object.last_name, object.sur_name, object.active, object.img_url, object.location, object.modif_date, object.id],
            function () {
                return db.query("select * from social_actor where id = ?", [object.id],
                    callback);
            });
    },

    delete: function (id, callback) {
        return db.query("delete from social_actor where id = ? ", [id], callback);
    }

};

module.exports = SocialActor;