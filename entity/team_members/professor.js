var db = require('../../dbconnection');

var Professor = {

    getAll: function (callback) {
        return db.query("select * from professor order by name asc", callback);
    },

    getActive: function (callback) {
        return db.query("select * from professor where active = true order by name asc", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from professor where id = ?", [id], callback);
    },

    getByName: function (name, callback) {
        return db.query("select * from professor where name = ?", name, callback);
    },
    
    create: function (object, callback) {
        return db.query("insert into professor (name, last_name, sur_name, active, img_url, creation_date) " +
            "values(?, ?, ?, ?, ?, ?)", [object.name, object.last_name, object.sur_name, object.active, object.img_url, object.creation_date], function () {
            return db.query("select * from professor where name = ?", [object.name], callback);
        });
    },

    update: function (object, callback) {
        return db.query("update professor set name = ?, last_name = ?, sur_name = ?, active = ?, " +
            "img_url = ?, modif_date = ? where id = ?",
            [object.name, object.last_name, object.sur_name, object.active, object.img_url, object.modif_date, object.id],
            function () {
                return db.query("select * from professor where id = ?", [object.id],
                    callback);
            });
    },

    delete: function (id, callback) {
        return db.query("delete from professor where id = ? ", [id], callback);
    }

};

module.exports = Professor;