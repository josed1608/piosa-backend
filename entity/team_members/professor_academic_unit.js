var db = require('../../dbconnection');

var ProfessorAcademicUnit = {

    getAll: function (callback) {
        return db.query("select * from professor_academic_unit order by id asc", callback);
    },

    create: function (object, callback) {
        return db.query("insert into professor_academic_unit (professor_id, unit_id) values(?, ?)",
            [object.professor_id, object.unit_id], function () {
                return db.query("select * from professor_academic_unit where professor_id = ? and unit_id = ?",
                    [object.professor_id, object.unit_id], callback);
            });
    },

    getDataByProfessor: function (id, callback) {
        return db.query("select professor_academic_unit.professor_id, professor_academic_unit.unit_id, " +
            "academic_unit.name from professor_academic_unit join academic_unit " +
            "on academic_unit.id = professor_academic_unit.unit_id where professor_id = ?", [id], callback);
    },

    delete: function (professor_id, unit_id, callback) {
        return db.query("delete from professor_academic_unit where professor_id = ? and unit_id = ?",
            [professor_id, unit_id], callback);
    }

};

module.exports = ProfessorAcademicUnit;