var db = require('../../dbconnection');

var WorkTeamProfessor = {

    getAll: function (callback) {
        return db.query("select * from work_team_professor order by professor_id asc", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from work_team_professor where id = ?", [id], callback);
    },

    getDataByTeamId: function (id, callback) {
        return db.query("select work_team_professor.team_id, work_team_professor.professor_id," +
            "professor.name, professor.last_name, professor.sur_name, " +
            "professor.img_url, professor.description "+
            "from work_team_professor " +
            "join professor on professor.id = work_team_professor.professor_id " +
            "where team_id = ?", [id], callback);
    },

    create: function (object, callback) {
        return db.query("insert into work_team_professor (team_id, professor_id) " +
            "values(?, ?)", [object.team_id, object.professor_id], function () {
            return db.query("select * from work_team_professor where team_id = ? and professor_id = ?",
                [object.team_id, object.professor_id], callback);
        });
    },

    delete: function (team, professor, callback) {
        return db.query("delete from work_team_professor where team_id = ? and professor_id = ?",
            [team, professor], callback);
    }

};

module.exports = WorkTeamProfessor;