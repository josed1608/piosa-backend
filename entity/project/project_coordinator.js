var db = require('../../dbconnection');

var ProjectCoordinator = {

    getAll: function (callback) {
        return db.query("select * from project_coordinator order by id asc", callback);
    },

    create: function (object, callback) {
        return db.query("insert into project_coordinator (project_id, coordinator_id) values(?, ?)",
            [object.project_id, object.coordinator_id],
            // function () {
            //     return db.query("select * from project_coordinator where project_id = ? and coordinator_id = ?", [object.project_id, object.coordinator_id],
        callback);
            // });
    },


    getByProjAndCoordinator: function (project, coordinator, callback) {
        return db.query("select * from project_coordinator where project_id = ? and coordinator_id = ?", [project, coordinator], callback);
    },


    getDataByProject: function (id, callback) {
        return db.query("select project_coordinator.project_id, project_coordinator.coordinator_id," +
            "professor.name, professor.last_name, professor.sur_name from project_coordinator " +
            "join professor on professor.id = project_coordinator.coordinator_id " +
            "where project_id = ?", [id], callback);
    },


    getDataByCollaborator: function (id, callback) {
        return db.query("select project.id, project.name from project_coordinator " +
            "join project on project_coordinator.project_id = project_coordinator.project_id " +
            "where coordinator_id = ?", [id], callback);
    },

    delete: function (project_id, coordinator_id, callback) {
        return db.query("delete from project_coordinator where project_id = ? and coordinator_id = ?", [project_id, coordinator_id], callback);
    }

};

module.exports = ProjectCoordinator;