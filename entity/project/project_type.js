var db = require('../../dbconnection');

var ProjectType = {

    getAll: function (callback) {
        return db.query("select * from project_type order by id asc", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from project_type where id = ?", [id], callback);
    },

    create: function (object, callback) {
        return db.query("insert into project (id, name, description, img_url, active, type, start_date, end_date, creation_date) " +
            "values(?, ?, ?, ?, ?, ?, ?, ?, ?)", [object.id, object.name, object.description, object.img_url, object.active,
                object.type, object.start_date, object.end_date, object.creation_date],
            function () {
                return db.query("select * from project where id = ?", [object.id], callback);
            });
    },


    getByName: function (name, callback) {
        return db.query("select * from project_type where name = ?", name, callback);
    }
};

module.exports = ProjectType;