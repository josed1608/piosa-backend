var db = require('../../dbconnection');

var Advance = {

    getAll: function (callback) {
        return db.query("select * from advance order by id asc", callback);
    },

    getActive: function (callback) {
        return db.query("select * from advance where active = true order by id asc", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from advance where id = ?", [id], callback);
    },

    getByTitle: function (title, callback) {
        return db.query("select * from advance where title = ?", title, callback);
    },

    create: function (object, callback) {
        return db.query("insert into advance (title, description, project_id, active, creation_date) " +
            "values(?, ?, ?, ?, ?)", [object.title, object.description, object.project_id, object.active, object.creation_date]
            // , function () {
            // return db.query("select * from advance where title = ?", [object.title],
            , callback);
        // });
    },

    update: function (object, callback) {
        return db.query("update advance set title = ?, description = ?, project_id = ?, active = ?, modif_date = ? where id = ?",
            [object.title, object.description, object.project_id, object.active, object.modif_date, object.id],
            function () {
                return db.query("select * from advance where id = ?", [object.id],
                    callback);
            });
    },

    delete: function (id, callback) {
        return db.query("delete from advance where id = ? ", [id], callback);
    }

};

module.exports = Advance;