var db = require('../../dbconnection');

var FacebookFeed = {

    getAll: function (callback) {
        return db.query("select * from facebook_feed order by created_time desc", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from facebook_feed where id = ?", [id], callback);
    },

    getByTitle: function (title, callback) {
        return db.query("select * from facebook_feed where title = ?", title, callback);
    },

    create: function (object, callback) {
        return db.query("insert into facebook_feed (title, description, proyect_id, creation_date) " +
            "values(?, ?, ?, ?)", [object.title, object.description, object.proyect_id, object.creation_date], function () {
            return db.query("select * from facebook_feed where title = ?", [object.title], callback);
        });
    },

    bulkCreate: function (objects, callback) {
        return db.query("delete from facebook_feed", function () {
            return db.query("insert into facebook_feed (id, message, link, full_picture, created_time) " +
                "values ?", [objects],
                function () {
                    return db.query("select * from facebook_feed",
                        callback);
                });
        });
    },

    update: function (object, callback) {
        return db.query("update facebook_feed set name = ? , email = ? where id = ?",
            [object.name, object.email, object.id], callback);
    },

    delete: function (id, callback) {
        return db.query("delete from facebook_feed where id = ? ", [id], callback);
    },

    dropAll: function (callback) {
        return db.query("delete from facebook_feed", callback);
    }


};

module.exports = FacebookFeed;