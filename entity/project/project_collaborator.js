var db = require('../../dbconnection');

var ProjectCollaborator = {

    getAll: function (callback) {
        return db.query("select * from project_collaborator order by id asc", callback);
    },

    create: function (object, callback) {
        return db.query("insert into project_collaborator (project_id, collaborator_id) values(?, ?)",
            [object.project_id, object.collaborator_id], function () {
                return db.query("select * from project_collaborator where project_id = ? and collaborator_id = ?",
                    [object.project_id, object.collaborator_id], callback);
            });
    },

    getByProjAndCollaborator: function (project, collaborator, callback) {
        return db.query("select * from project_collaborator where project_id = ? and collaborator_id = ?", [project, collaborator], callback);
    },

    getDataByProject: function (id, callback) {
        return db.query("select project_collaborator.project_id, project_collaborator.collaborator_id, " +
            "social_actor.name, social_actor.last_name, social_actor.sur_name from project_collaborator " +
            "join social_actor on social_actor.id = project_collaborator.collaborator_id " +
            "where project_id = ?", [id], callback);
    },


    getDataByCollaborator: function (id, callback) {
        return db.query("select project.id, project.name from project_collaborator " +
            "join project on project_collaborator.project_id = project_collaborator.project_id " +
            "where collaborator_id = ?", [id], callback);
    },

    delete: function (project_id, collaborator_id, callback) {
        return db.query("delete from project_collaborator where project_id = ? and collaborator_id = ?",
            [project_id, collaborator_id], callback);
    }


};

module.exports = ProjectCollaborator;