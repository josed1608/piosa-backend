var db = require('../../dbconnection');

var ProjectLocation = {

    getAll: function (callback) {
        return db.query("select * from project_location order by id asc", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from project_location where id = ?", [id], callback);
    },

    getByProject: function (id, callback) {
        return db.query("select * from project_location where project_id = ?", [id], callback);
    },

    create: function (object, callback) {
        return db.query("insert into project_location (project_id, latitude, longitude, tag) values(?, ?, ?, ?)",
            [object.project_id, object.latitude, object.longitude, object.tag],
            function () {
                return db.query("select * from project_location where latitude = ? and longitude = ?",
                    [object.latitude, object.longitude],
                        callback);
            });
    },

    update: function (object, callback) {
        return db.query("update project_location set latitude = ?, longitude = ?, tag = ? where id = ? and project_id = ?",
            [object.latitude, object.longitude, object.tag, object.id, object.project_id], function () {
                return db.query("select * from project_location where id = ? and project_id = ?", [object.id, object.project_id], callback);
            });
    },

    delete: function (id, callback) {
        return db.query("delete from project_location where id = ? ", [id], callback);
    }

};

module.exports = ProjectLocation;