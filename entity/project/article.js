var db = require('../../dbconnection');

var Article = {

    getAll: function (callback) {
        return db.query("select * from article order by id asc", callback);
    },

    getActive: function (n, callback) {
        return db.query("select * from article where active = true order by id asc limit ?", [n], callback);
    },

    getById: function (id, callback) {
        return db.query("select * from article where id = ?", [id], callback);
    },

    getByTitle: function (title, callback) {
        return db.query("select * from article where title = ?", title, callback);
    },

    create: function (object, callback) {
        return db.query("insert into article (title, resume, content, active, link, project_id, img_url, creation_date) " +
            "values(?, ?, ?, ?, ?, ?, ?, ?)",
            [object.title, object.resume, object.content, object.active, object.link, object.project_id,
                object.img_url, object.creation_date],
            // function () {
            //     return db.query("select * from article where title = ?", [object.title],
                    callback);
            // });
    },

    update: function (object, callback) {
        return db.query("update article set title = ?, resume = ?, content = ?, active = ?,link = ?, project_id = ?, " +
            "img_url = ?, modif_date = ? where id = ?",
            [object.title, object.resume, object.content, object.active, object.link, object.project_id, object.img_url, object.modif_date, object.id],
            function () {
                return db.query("select * from article where id = ?", [object.id],
                    callback);
            });
    },

    delete: function (id, callback) {
        return db.query("delete from article where id = ? ", [id], callback);
    }

};

module.exports = Article;