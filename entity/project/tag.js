var db = require('../../dbconnection');

var Tag = {

    getAll: function (callback) {
        return db.query("select * from tag order by id asc", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from tag where id = ?", [id], callback);
    },

    getByName: function (name, callback) {
        return db.query("select * from tag where name = ?", name, callback);
    },

    create: function (object, callback) {
        return db.query("insert into project (id, name, description, img_url, active, type, start_date, end_date, creation_date) " +
            "values(?, ?, ?, ?, ?, ?, ?, ?, ?)", [object.id, object.name, object.description, object.img_url, object.active,
                object.type, object.start_date, object.end_date, object.creation_date],
            function () {
                return db.query("select * from project where id = ?", [object.id], callback);
            });
    },

    update: function (object, callback) {
        return db.query("update project set name = ?, description = ?, img_url = ?, type = ?, active = ?, start_date = ?, " +
            "end_date = ?, modif_date = ? where id = ?",
            [object.name, object.description, object.img_url, object.type,  object.active, object.start_date, object.start_date, object.end_date, object.id],
            function () {
                return db.query("select * from project where id = ?", [object.id],
                    callback);
            });
    },


    delete: function (id, callback) {
        return db.query("delete from tag where id = ? ", [id], callback);
    }



};

module.exports = Tag;