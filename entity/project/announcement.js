var db = require('../../dbconnection');

var Announcement = {

    getAll: function (callback) {
        return db.query("select * from announcement order by id asc", callback);
    },

    getActive: function (callback) {
        return db.query("select * from announcement where active = true order by id asc", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from announcement where id = ?", [id], callback);
    },

    getByTitle: function (name, callback) {
        return db.query("select * from announcement where title = ?", name, callback);
    },

    create: function (object, callback) {
        return db.query("insert into announcement (title, description, img_url, project_id, active, creation_date) " +
            "values(?, ?, ?, ?, ?, ?)",
            [object.title, object.description, object.img_url, object.project_id, object.active, object.creation_date],
            // function () {
            //     return db.query("select * from announcement where title = ?", [object.title],
        callback);
        //     });
    },

    update: function (object, callback) {
        return db.query("update announcement set title = ?, description = ?, img_url = ?, active = ?, project_id = ?, modif_date = ? where id = ?",
            [object.title, object.description, object.img_url, object.active, object.project_id, object.modif_date, object.id],
            function () {
                return db.query("select * from announcement where id = ?", [object.id],
                    callback);
            });
    },

    delete: function (id, callback) {
        return db.query("delete from announcement where id = ? ", [id], callback);
    }

};

module.exports = Announcement;