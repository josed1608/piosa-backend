var db = require('../../dbconnection');

var Project = {

    getAll: function (callback) {
        return db.query("select * from project order by name asc" , callback);
    },

    getActive: function (callback) {
        return db.query("select * from project where active = true order by name asc" , callback);
    },

    getById: function (id, callback) {
        return db.query("select * from project where id = ?", [id], callback);
    },

    getByName: function (name, callback) {
        return db.query("select * from project where name = ?", name, callback);
    },

    getByType: function (type, callback) {
        return db.query("select * from project where type = ? order by name asc", type, callback);
    },

    getByProfessor: function (id, callback) {
        return db.query("select project.name, project.id from project_coordinator " +
            "join project on project.id = project_coordinator.project_id " +
            "where project_coordinator.coordinator_id = ?", id, callback);
    },

    getByCollaborator: function (id, callback) {
        return db.query("select project.name, project.id from project_collaborator " +
            "join project on project.id = project_collaborator.project_id " +
            "where project_collaborator.collaborator_id = ?", id, callback);
    },

    getActiveByType: function (type, callback) {
        return db.query("select * from project where type = ? and active = true order by name asc", type, callback);
    },

    create: function (object, callback) {
        return db.query("insert into project (id, name, description, img_url, active, type, start_date, end_date, creation_date) " +
            "values(?, ?, ?, ?, ?, ?, ?, ?, ?)", [object.id, object.name, object.description, object.img_url, object.active,
                object.type, object.start_date, object.end_date, object.creation_date],
            function () {
                return db.query("select * from project where id = ?", [object.id], callback);
            });
    },

    update: function (object, callback) {
        return db.query("update project set name = ?, description = ?, img_url = ?, type = ?, active = ?, start_date = ?, " +
            "end_date = ?, modif_date = ? where id = ?",
            [object.name, object.description, object.img_url, object.type,  object.active, object.start_date, object.start_date, object.end_date, object.id],
            function () {
                return db.query("select * from project where id = ?", [object.id],
                    callback);
            });
    },

    delete: function (id, callback) {
        return db.query("delete from project where id = ? ", [id], callback);
    }

};

module.exports = Project;