var db = require('../../dbconnection');

var Event = {

    getAll: function (callback) {
        return db.query("select * from event order by id asc", callback);
    },

    getActive: function (callback) {
        return db.query("select * from event where active = true order by id asc", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from event where id = ?", [id], callback);
    },

    getByName: function (name, callback) {
        return db.query("select * from event where name = ?", name, callback);
    },

    create: function (object, callback) {
        return db.query("insert into event (title, description, img_url, active, start, end, project_id, creation_date) " +
            "values(?, ?, ?, ?, ?, ?, ?, ?)",
            [object.title, object.description, object.img_url, object.active, object.start, object.end, object.project_id, object.creation_date],
            function () {
                return db.query("select * from event where creation_date = ?", [object.creation_date],
                    callback);
            });
    },

    update: function (object, callback) {
        return db.query("update event set title = ?, description = ?, active = ?, img_url = ?, start = ?, end = ?," +
            "project_id = ?, modif_date = ? where id = ?",
            [object.title, object.description, object.active, object.img_url, object.start, object.end, object.project_id, object.modif_date, object.id],
            function () {
                return db.query("select * from event where id = ?", [object.id],
                    callback);
            });
    },

    delete: function (id, callback) {
        return db.query("delete from event where id = ? ", [id], callback);
    }

};

module.exports = Event;