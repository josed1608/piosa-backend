var db = require('../../dbconnection');

var RoleType = {

    getAll: function (callback) {
        return db.query("select * from role_type order by id asc", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from role_type where id = ?", id, callback);
    }

};

module.exports = RoleType;