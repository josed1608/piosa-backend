var db = require('../../dbconnection');

var AdminRole = {

    getAll: function (callback) {
        return db.query("select * from admin_role", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from admin_role where id = ?", id, callback);
    },

    getByAdminAndEntity: function (object, callback) {
        return db.query("select * from admin_role where admin_id = ? and entity_id = ?",
            [object.admin_id, object.entity_id], callback);
    },

    getByAdminId: function (id, callback) {
        return db.query("select * from admin_role where admin_id = ?", id, callback);
    },

    getDataByAdminId: function (id, callback) {
        return db.query("select entity_id, role_type, entity.name as entity, role_type.name as role from admin_role join role_type on admin_role.role_type = role_type.id " +
            "join entity on admin_role.entity_id = entity.id where admin_id = ?", id, callback);
    },

    create: function (object, callback) {
        console.log(object.admin_id);
        console.log(object.entity_id);
        return db.query("insert into admin_role (admin_id, entity_id, role_type) " +
            "values(?, ?, ?)", [object.admin_id, object.entity_id, object.role_type],
            // function () {
            // return db.query("select * from admin_role where admin_id = ? and entity_id = ?", [object.admin_id, object.entity_id],
                callback);
        // });
    },

    updateRoleType: function (object, callback) {
        return db.query("update admin_role set role_type = ? where admin_id = ? and entity_id = ?",
            [object.role_type, object.admin_id, object.entity_id], callback);
    },

    delete: function (object, callback) {
        return db.query("delete from admin_role where admin_id = ? and entity_id = ?", [object.admin_id, object.entity_id], callback);
    }


};

module.exports = AdminRole;