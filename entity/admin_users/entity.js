var db = require('../../dbconnection');

var Entity = {

    getAll: function (callback) {
        return db.query("select * from entity order by id asc", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from entity where id = ?", id, callback);
    }

};

module.exports = Entity;