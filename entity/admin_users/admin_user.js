var db = require('../../dbconnection');

var AdminUser = {

    getAll: function (callback) {
        return db.query("select * from admin_user", callback);
    },

    getActive: function (callback) {
        return db.query("select * from admin_user where active = true", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from admin_user where id = ?", [id], callback);
    },

    getByEmail: function (email, callback) {
        return db.query("select * from admin_user where email = ?", [email], callback);
    },

    register: function (object, callback) {
        return db.query("insert into admin_user (name, email, password, active, creation_date) " +
            "values(?, ?, ?, ?, ?)", [object.name, object.email, object.password, object.active, object.creation_date], function () {
            return db.query("select * from admin_user where email = ?", [object.email], callback);
        });
    },

    create: function (object, callback) {
        return db.query("insert into admin_user (name, email, active, creation_date) " +
            "values(?, ?, ?, ?)", [object.name, object.email, object.active, object.creation_date], function () {
            return db.query("select * from admin_user where email = ?", [object.email], callback);
        });
    },

    update: function (object, callback) {
        return db.query("update admin_user set name = ? , email = ?, active = ? where id = ?",
            [object.name, object.email, object.active, object.id], callback);
    },

    delete: function (id, callback) {
        return db.query("delete from admin_user where id = ? ", [id], callback);
    }
};

module.exports = AdminUser;