var db = require('../../dbconnection');

var AdminType = {

    getAll: function (callback) {
        return db.query("select * from admin_type", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from admin_type where id = ?", id, callback);
    }

};

module.exports = AdminType;