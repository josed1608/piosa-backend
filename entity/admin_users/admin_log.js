var db = require('../../dbconnection');

var AdminLog = {

    getAll: function (callback) {
        return db.query("select * from admin_log", callback);
    },

    getById: function (id, callback) {
        return db.query("select * from admin_log where id = ?", id, callback);
    },

    getByDate: function (id, callback) {
        return db.query("select * from admin_log where creation_date = ?", id, callback);
    },

    getByUser: function (id, callback) {
        return db.query("select * from admin_log where created_by = ?", id, callback);
    },

    getByEntity: function (id, callback) {
        return db.query("select * from admin_log where entity_id = ?", id, callback);
    },

    create: function (object, callback) {
        return db.query("insert into admin_log (creation_date, created_by, entity_id, action, detail) " +
            "values(?, ?, ?, ?, ?)", callback);
    },

    deleteById: function (id, callback) {
        return db.query("delete from admin_log where id = ?", [id], callback);
    },

    update: function (object, callback) {
        return db.query("update admin_log set name = ? where id = ?", [object.name, object.id], callback);
    }


};

module.exports = AdminLog;