var db = require('../../dbconnection');

var AdminToken = {

    getAll: function (callback) {
        return db.query("select * from admin_user_token order by expiration_date asc", callback);
    },

    getByUser: function (id, callback) {
        return db.query("select * from admin_user_token where admin_id = ?", id, callback);
    },

    get: function (admin, token, callback) {
        return db.query("select * from admin_user_token where token = ?", [admin, token], callback);
    },

    create: function (admin, token, exp, used, callback) {
        return db.query("insert into admin_user_token (admin_id, token, expiration_date, used) values(?, ?, ?, ?)",
            [admin, token, exp, used], function () {
                return db.query("select * from admin_user_token where admin_id = ? and token = ? and expiration_date = ?",
                    [admin, token, exp], callback);
            });
    },

    update: function (object, callback) {
        return db.query("update admin_user_token set expiration_date = ?, used = ? where id = ?",
            [object.expiration_date, object.used, object.admin_id, object.token], function () {
                return db.query("select * from admin_user_token where admin_id = ? and token = ? and expiration_date = ?",
                    [object.admin_id, object.token, object.expiration_date], callback);
            });
    },

    use: function (used, admin , token, callback) {
        return db.query("update admin_user_token set used = ? where admin = ? and token = ?",
            [used, admin, token], function () {
                return db.query("select * from admin_user_token where admin_id = ? and token = ?",
                    [admin, token], callback);
            });
    },

    delete: function (id, callback) {
        return db.query("delete from admin_user_token where admin_id = ? and token = ? and expiration_date = ?",
            [object.admin_id, object.token, object.expiration_date], callback);
    }

};

module.exports = AdminToken;